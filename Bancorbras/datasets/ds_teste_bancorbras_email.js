function createDataset(fields, constraints, sortFields) {

	var emailFlu = "";

	if (constraints != null) {
        for (var c = 0; c < constraints.length; c++) {
            if (constraints[c].fieldName == "mail") {
                emailFlu = constraints[c].initialValue;
            }
        }
    }

	var dataset = DatasetBuilder.newDataset();
	try{
	    var clientService = fluigAPI.getAuthorizeClientService();
	    var data = {                                                   
	        companyId : getValue("WKCompany") + '',
	        serviceCode : 'WSTesteBancorBras',                     
	        endpoint : '/GETFUNCFER?CHAVE=c8ZKRwZz8HyPMPQIlziD&EMAIL=atila.almeida@bancorbras.com.br',  
	        method : 'get',                                        
	    }                                                          
	    var vo = clientService.invoke(JSON.stringify(data));
	 
	    if(vo.getResult()== null || vo.getResult().isEmpty()){
	        throw new Exception("Retorno está vazio");
	    }else{
	        log.info(vo.getResult());
			dataset.addColumn("Nome");
	        dataset.addColumn("Departamento");
	        dataset.addColumn("DESDEPAR");
	        dataset.addColumn("Filial");
	        dataset.addColumn("Matricula");
	        dataset.addColumn("TurnoTrabalho");
			dataset.addColumn("AcordoColetivo");
	        dataset.addColumn("Funcao");
	        dataset.addColumn("EmailGestor");
	        dataset.addColumn("Gestor");
	        dataset.addColumn("UmFeriasStatus");
			dataset.addColumn("UmInicioDatabase");
			dataset.addColumn("UmFimDatabase");
	        dataset.addColumn("UmFeriasVencer");
	        dataset.addColumn("UmFeriasDireito");
	        dataset.addColumn("UM_DT_INI_PROG1");
	        dataset.addColumn("UM_DIAS_PROG1");
			dataset.addColumn("UM_DIAS_AB_PROG1");
	        dataset.addColumn("UM_DT_INI_PROG2");
	        dataset.addColumn("UM_DIAS_PROG2");
	        dataset.addColumn("UM_DIAS_AB_PROG2");
	        dataset.addColumn("UM_SALDO_DIAS_FER");
			dataset.addColumn("DOIS_FER_STATUS");
			dataset.addColumn("DOIS_INI_DATABASE");
	        dataset.addColumn("DOIS_FIM_DATABASE");
	        dataset.addColumn("DOIS_FER_A_VENCER");
	        dataset.addColumn("DOIS_FER_DIREITO");
	        dataset.addColumn("DOIS_DT_INI_PROG1");
			dataset.addColumn("DOIS_DIAS_PROG1");
	        dataset.addColumn("DOIS_DIAS_AB_PROG1");
	        dataset.addColumn("DOIS_DT_INI_PROG2");
	        dataset.addColumn("DOIS_DIAS_PROG2");
	        dataset.addColumn("DOIS_DIAS_AB_PROG2");
			dataset.addColumn("DOIS_SALDO_DIAS_FER");
			dataset.addColumn("TRES_FER_STATUS");
	        dataset.addColumn("TRES_INI_DATABASE");
	        dataset.addColumn("TRES_FIM_DATABASE");
	        dataset.addColumn("TRES_FER_A_VENCER");
	        dataset.addColumn("TRES_FER_DIREITO");
			dataset.addColumn("TRES_DT_INI_PROG1");
	        dataset.addColumn("TRES_DIAS_PROG1");
	        dataset.addColumn("TRES_DIAS_AB_PROG1");
	        dataset.addColumn("TRES_DT_INI_PROG2");
	        dataset.addColumn("TRES_DIAS_PROG2");
			dataset.addColumn("TRES_DIAS_AB_PROG2");
			dataset.addColumn("TRES_SALDO_DIAS_FER");	
			

	        var json = JSON.parse(vo.getResult());
	    	
			dataset.addRow([json.NOME,
							json.DEPART,
							json.DESDEPAR,
							json.FILIAL, 
							json.MATRIC,
							json.TURTRAB, 
							json.EMPREST_ACT, 
							json.FUNCAO, 
							json.GESTOR, 
							json.EMAIL_GESTOR, 
							json.UM_FER_STATUS, 
							json.UM_INI_DATABASE, 
							json.UM_FIM_DATABASE, 
							json.UM_FER_A_VENCER, 
							json.UM_FER_DIREITO, 
							json.UM_DT_INI_PROG1, 
							json.UM_DIAS_PROG1, 
							json.UM_DIAS_AB_PROG1, 
							json.UM_DT_INI_PROG2, 
							json.UM_DIAS_PROG2, 
							json.UM_DIAS_AB_PROG2, 
							json.UM_SALDO_DIAS_FER, 
							json.DOIS_FER_STATUS, 
							json.DOIS_INI_DATABASE, 
							json.DOIS_FIM_DATABASE, 
							json.DOIS_FER_A_VENCER,
							json.DOIS_FER_DIREITO, 
							json.DOIS_DT_INI_PROG1,
							json.DOIS_DIAS_PROG1, 
							json.DOIS_DIAS_AB_PROG1, 
							json.DOIS_DT_INI_PROG2, 
							json.DOIS_DIAS_PROG2, 
							json.DOIS_DIAS_AB_PROG2, 
							json.DOIS_SALDO_DIAS_FER, 
							json.TRES_FER_STATUS, 
							json.TRES_INI_DATABASE, 
							json.TRES_FIM_DATABASE, 
							json.TRES_FER_A_VENCER, 
							json.TRES_FER_DIREITO, 
							json.TRES_DT_INI_PROG1, 
							json.TRES_DIAS_PROG1, 
							json.TRES_DIAS_AB_PROG1, 
							json.TRES_DT_INI_PROG2, 
							json.TRES_DIAS_PROG2, 
							json.TRES_DIAS_AB_PROG2, 
							json.TRES_SALDO_DIAS_FER]);
	    }
	} catch(err) {
	    throw new Exception(err);
	}
	return dataset;
}