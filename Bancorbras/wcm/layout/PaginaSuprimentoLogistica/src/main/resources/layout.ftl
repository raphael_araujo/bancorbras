
   <#import "/wcm.ftl" as wcm/>
    <!--    <@wcm.header authenticated="true"/>   -->
    
    <!-- WCM Wrapper content -->
    <div class="wcm-wrapper-content1">
    <!--  <@wcm.menu /> -->
    
    <!-- Wrapper -->
    <div class="wcm-all-content1" style="padding: [0px, 0px, 0px, 0px];">
      <div id="wcm-content" class="clearfix wcm-background"> 
        
        <!-- Onde deverá estar a barra de formatação --> 
        <#if pageRender.isEditMode()=true>
        <div name="formatBar" id="formatBar"></div>
        <!-- Div geral --> 
        <!-- Há CSS distinto para Edição/Visualização -->
        <div id="edicaoPagina" class="clearfix"> <#else>
          <div id="visualizacaoPagina" class="clearfix"> </#if>
            <div class="fluig-style-guide"> 
              <!-- Titulo da página -->
           
              
              <!-- Slot 1 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull1">
                <@wcm.renderSlot id="SlotA" editableSlot="true"/>
                
                <!-- Inicio Header -->
                <div class="page-header">
                  <header class="main-header clearfix">
                    <div class="wrapper">
                      <div class="row"> 
                        
                        <!-- Logo Centro -->
                        <div class="col-md-12"> <img src="http://bancorbras.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTMxMCZ2ZXI9MTAwMCZmaWxlPWhlYWQuanBnJmNyYz0yOTg3NDk5MjI0JnNpemU9MC4xNjgwNzEmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg" class="img-fluid" alt="Responsive image"> </div>
                      </div>
                    </div>
                  </header>
                </div>
                <!-- Fim Header --> 
                
              </div>
              
              <!-- Slot 2 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull2">
                <@wcm.renderSlot id="SlotNavBar" editableSlot="true"/>
               
                
               
               
               
               
              </div>
              
              <!-- Slot 3 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull3">
                <@wcm.renderSlot id="SlotBanner" editableSlot="true"/>
                <div class="row"> 
                  
                  <!-- Logo Centro -->
                  <div class="col-md-12"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTMwNyZ2ZXI9MTAwMCZmaWxlPXN1cHJpbWVudG9wYWdlLmpwZyZjcmM9Mzc2NTU4NjQ1NyZzaXplPTAuMjM4MTUzJnVJZD0yNTYmZlNJZD0xJnVTSWQ9MSZkPWZhbHNlJnRrbj0mcHVibGljVXJsPXRydWU=.jpg" class="img-fluid" alt="Responsive image"> </div>
                </div>
              </div>
              
              <!-- Slot 4 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull4">
                <@wcm.renderSlot id="SlotD" editableSlot="true"/>
                
                
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel panel-info">
                        <div class="panel-heading text-center" style="background-color: #88cce1">
                          <h3 class="panel-title"> <font size="+1" style="color: #FFFFFF"> <strong>Confira a seguir os serviços que podem ser solicitados através de cada ferramenta do CSC.</strong> </font> </h3>
                        </div>
                        <div class="panel-body" style="background-color: #effafd">
                          <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Pesquisar" />
                        </div>
                        <table class="table" id="dev-table">
                          <thead>
                          </thead>
                          <tbody>
                            <!-- Linhas para editar opcoes tabela-->
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Cadastro Patrimônio</strong></font></a>
                                <div id="collapse1" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                                </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Consulta Patrimônio</strong></font></a> 
                                <!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse2" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                                </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Empréstimo Patrimônio</strong></font></a> 
                                <!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse3" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                                </div>
                                </td>
                            </tr>
                            
                        
                            
                            <!-- Fim Linhas para editar opcoes tabela-->
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                
                
                
              </div>
              
              <!-- Slot 5 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull5">
                <@wcm.renderSlot id="SlotE" editableSlot="true"/>
                
                
               
                
                
                
                
                
                
                
              </div>
            </div>
          </div>
          <!-- FIM DAS WIDGETS DO LAYOUT -->
          <!--  <@wcm.footer layoutuserlabel="wcm.layoutdefaultpublic.user" /> -->
        </div>
      </div>
    </div>
