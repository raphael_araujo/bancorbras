 <#import "/wcm.ftl" as wcm/> 
     <!--    <@wcm.header authenticated="true"/>   -->  
    
    <!-- WCM Wrapper content -->
    <div class="wcm-wrapper-content1">
    <!--  <@wcm.menu /> --> 
    
    <!-- Wrapper -->
    <div class="wcm-all-content1" style="padding: [0px, 0px, 0px, 0px];">
      <div id="wcm-content" class="clearfix wcm-background"> 
        
        <!-- Onde deverá estar a barra de formatação --> 
        <#if pageRender.isEditMode()=true>
        <div name="formatBar" id="formatBar"></div>
        <!-- Div geral --> 
        <!-- Há CSS distinto para Edição/Visualização -->
        <div id="edicaoPagina" class="clearfix"> <#else>
          <div id="visualizacaoPagina" class="clearfix"> </#if>
            <div class="fluig-style-guide"> 
              <!-- Titulo da página --> 
              
              <!-- Slot 1 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull1">
                <@wcm.renderSlot id="SlotA" editableSlot="true"/>
                
                <!-- Inicio Header -->
                <div class="page-header">
                  <header class="main-header clearfix">
                    <div class="wrapper">
                      <div class="row"> 
                        
                        <!-- Logo Centro -->
                        <div class="col-md-12"> <img src="http://bancorbras.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTMxMCZ2ZXI9MTAwMCZmaWxlPWhlYWQuanBnJmNyYz0yOTg3NDk5MjI0JnNpemU9MC4xNjgwNzEmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg" class="img-fluid" alt="Responsive image"> </div>
                      </div>
                    </div>
                  </header>
                </div>
                <!-- Fim Header --> 
                
              </div>
              
              <!-- Slot Barra Navegacao -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull2">
                <@wcm.renderSlot id="SlotNavBar" editableSlot="true"/>
              </div>
              
              <!-- Slot 3 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull3">
                <@wcm.renderSlot id="SlotBanner" editableSlot="true"/>
                <div class="row"> 
                  
                  <!-- Logo Centro -->
                  <div class="col-md-12"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI5MCZ2ZXI9MTAwMCZmaWxlPUZBUVNMQS5qcGcmY3JjPTMzOTM5MDQxMTImc2l6ZT0wLjIzMzU0MSZ1SWQ9MjU2JmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.jpg" class="img-fluid" alt="Responsive image"> </div>
                </div>
              </div>
              
              <!-- Slot 4 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull4">
                <@wcm.renderSlot id="SlotD" editableSlot="true"/>
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel panel-info">
                        <div class="panel-heading text-center" style="background-color: #88cce1">
                          <h3 class="panel-title"> <font size="+1" style="color: #FFFFFF"> <strong>Perguntas Frequentes</strong> </font> </h3>
                        </div>
                        <div class="panel-body" style="background-color: #effafd">
                          <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Pesquisar" />
                        </div>
                        <table class="table" id="dev-table">
                          <thead>
                          </thead>
                          <tbody>
                            <!-- Linhas para editar opcoes tabela-->
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> O que é o consórcio?</strong></font></a>
                                <div id="collapse1" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title">&nbsp;</h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              
                                              <p>É a reunião de pessoas físicas e/ou jurídicas, em grupo com prazo de duração e número de cotas previamente determinados, promovida por uma administradora, com a finalidade de propiciar aos seus integrantes a aquisição de um bem por meio do autofinanciamento.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> O que é um grupo?</strong></font></a> 
                                <!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse2" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title">&nbsp;</h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              
                                              <p>Grupo é uma sociedade de fato, a qual será constituída na data de realização da primeira assembleia geral ordinária, por consorciados reunidos pela administradora, para o fim de propiciar a estes a aquisição de bem, conjunto de bens ou serviço por meio de autofinanciamento, com prazo de duração previamente estabelecido.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> As novas regras sobre consórcio valem para grupos antigos?</strong></font></a> 
                                <!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse3" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title">&nbsp;</h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              
                                              <p>As disposições da lei 11.795, de 2008, começaram a vigorar 120 dias após sua publicação. Assim, a nova legislação é aplicável apenas para os grupos formados a partir de 6.2.2009.
                                                
                                                Para os grupos formados até 5.2.2009, permanece válida a regulamentação anterior, observadas as disposições dos contratos firmados. No entanto, as assembleias gerais extraordinárias podem decidir pela adoção da nova legislação.</p>
                                              <p> Fonte: BACEN</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div></td>
                            </tr>
                            
                            <!-- Fim Linhas para editar opcoes tabela-->
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <!-- Slot 5 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull5">
                <@wcm.renderSlot id="SlotE" editableSlot="true"/>
              </div>
            </div>
          </div>
          <!-- FIM DAS WIDGETS DO LAYOUT --> 
          
        </div>
      </div>
    </div>