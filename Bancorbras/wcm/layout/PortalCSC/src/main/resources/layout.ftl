 <#import "/wcm.ftl" as wcm/> 
<!--    <@wcm.header authenticated="true"/>   --> 
    <!-- WCM Wrapper content -->
    <div class="wcm-wrapper-content1"> 
      
      <!-- Wrapper -->
      <div class="wcm-all-content1" style="padding: [0px, 0px, 0px, 0px];">
        <div id="wcm-content" class="clearfix wcm-background"> 
          
          <!-- Your content here --> 
          
          <!-- Onde deverá estar a barra de formatação --> 
          <#if pageRender.isEditMode()=true>
          <div name="formatBar" id="formatBar"> </div>
          <!-- Div geral --> 
          <!-- Há CSS distinto para Edição/Visualização -->
          <div id="edicaoPagina" class="clearfix"> <#else>
            <div id="visualizacaoPagina" class="clearfix"> </#if>
              <div class="fluig-style-guide"> 
                <!-- Slot Header -->
                <div class="row">
                  <div class="col-md-12">
                    <div class="editable-slot slotfull layout-1-1" id="slotFull1">
                      <@wcm.renderSlot id="SlotAHeader" editableSlot="true"/>
                      <!-- Inicio Header -->
                      <div class="page-header">
                        <header class="main-header clearfix">
                          <div class="wrapper">
                            <div class="row"> 
                              
                              <!-- Logo Centro -->
                              <div class="col-md-12"> <a href="http://bancorbras.fluig.com/wcmportalcsc"> <img src="http://bancorbras.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTMxMCZ2ZXI9MTAwMCZmaWxlPWhlYWQuanBnJmNyYz0yOTg3NDk5MjI0JnNpemU9MC4xNjgwNzEmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg" class="img-fluid" alt="Responsive image"> </a> </div>
                            </div>
                          </div>
                        </header>
                      </div>
                      <!-- Fim Header --> 
                      
                    </div>
                  </div>
                </div>
                <!-- FIm slot header -->
                
                <div class="row">
                  <div class="col-xs-12 col-lg-12">
                    <div class="editable-slot slotfull layout-1-1" id="slotFullSlide">
                      <@wcm.renderSlot id="SlotSlide" editableSlot="true"/>
                      
                      
                      
                      <div class="container" style="height: 400;">
    <!-- Indicators -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
            <div class="col-md-12">
            <a href="#" target="_blank">
                <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTIyOSZ2ZXI9MTAwMCZmaWxlPUJhbm5lcmwtQ1NDLUhvbWUuanBnJmNyYz0zMTA1NDkzNjcwJnNpemU9MC4zMTg1MzcmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg" class="img-fluid" alt="Saiba Mais">
                <!--   div class="carousel-caption">
                    <h3>Header of Slide 1</h3>
                    <p>Details of Slide 1.....</p>
                </div -->
                </a>
            </div>
            </div>
            <div class="item">
            <div class="col-md-12">
            <a target="_blank" href="http://bancorbras.fluig.com/portal/p/1/paginafaq">
                <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI4MSZ2ZXI9MTAwMCZmaWxlPUJhbm5lcmwtQ1NDLUZBUS5qcGcmY3JjPTIwODE3MjQ3OTEmc2l6ZT0wLjIxOTYwMSZ1SWQ9MjU2JmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.jpg" class="img-fluid" alt="FAQ">
                <!--  div class="carousel-caption">
                    <h3>Header of Slide 2</h3>
                    <p>Details of Slide 2. </p>
                </div -->
                </a>
            </div>
            
            </div>
                    
                      <div class="item">
            <div class="col-md-12">
            <a href="http://bancorbras.fluig.com/portal/p/1/paginanoticias">
                <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI5OCZ2ZXI9MTAwMCZmaWxlPUJhbm5lcmwtQ1NDLU5vdGljaWFzLmpwZyZjcmM9MjcwOTM1ODU4MCZzaXplPTAuMTg4MTkzJnVJZD0yNTYmZlNJZD0xJnVTSWQ9MSZkPWZhbHNlJnRrbj0mcHVibGljVXJsPXRydWU=.jpg" class="img-fluid" alt="Notícias">
                <!-- div class="carousel-caption">
                    <h3>Header of Slide3</h3>
                    <p>Details of Slide 3.</p>
                </div -->
                </a>
                </div>
            </div>
            
            
        </div>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Próximo</span>
        </a>
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
           
        </ol>
    </div>
</div>
                      
                      
                      
                      
                      
                      
                      
                      
                      
                    </div>
                  </div>
                </div>
                
                <!-- Slot Barra Navegacao -->
                <div class="row">
                  <div class="col-md-12"> 
                    <!-- Slot 1 -->
                    <div class="editable-slot slotfull layout-1-1" id="slotFull2">
                      <@wcm.renderSlot id="SlotBBarraNav" editableSlot="true"/>
                      
                      
                    </div>
                  </div>
                </div>
                
                <!-- FIM Slot Barra Navegacao -->
                
                <div class="row">
                  <div class="col-md-12"> 
                    <!-- Slot 1 -->
                    <div class="editable-slot slotfull layout-1-1" id="slotFull3">
                      <@wcm.renderSlot id="SlotC" editableSlot="true"/>
                      <div class="container-fluid padding-top">
                        <div class="col-md-4 col-sm-6">
                          <div class="box"
												style="background-image: url(&#39;http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTIyOCZ2ZXI9MTAwMCZmaWxlPUFzc2Vzc29yaWFKdXJpZGljYSslMjg2NDB4NDI3JTI5LmpwZyZjcmM9MTMyMjEwMzgyNCZzaXplPTAuMTM0MDMmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg&#39;)"> <a href="http://bancorbras.fluig.com/portal/p/1/assessoriajuridica" class="title">
                            <div class="title-g">Mais Informações</div>
                            </a>
                            <div class="legend-black"> <font face="verdana"> <b>ASSESSORIA JURÍDICA</b> </font> </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                          <div class="box"
												style="background-image: url(&#39;http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTIzNSZ2ZXI9MTAwMCZmaWxlPWNvbnRhYmlsaWRhZGUrJTI4NjQweDQyNyUyOS5qcGcmY3JjPTEwNzg3NTA0MjAmc2l6ZT0wLjEwODgyOCZ1SWQ9MjU2JmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.jpg&#39;);"> <a href="http://bancorbras.fluig.com/portal/p/1/contabilidade"  class="title">
                            <div class="title-g">Mais Informações</div>
                            </a>
                            <div class="legend-black"> <font face="verdana"> <b>CONTABILIDADE</b> </font> </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                          <div class="box"
												style="background-image: url(&#39;http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTIzOSZ2ZXI9MTAwMCZmaWxlPWZpbmFuJUMzJUE3YXMrJTI4NjQweDQyNyUyOS5qcGcmY3JjPTI0NjQxNjUzOTgmc2l6ZT0wLjEwNjU0NSZ1SWQ9MjU2JmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.jpg&#39;);"> <a href="http://bancorbras.fluig.com/portal/p/1/financas"  class="title">
                            <div class="title-g">Mais Informações</div>
                            </a>
                            <div class="legend-black"> <font face="verdana"> <b>FINANÇAS</b> </font> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12"> 
                    <!-- Slot 1 -->
                    <div class="editable-slot slotfull layout-1-1" id="slotFull4">
                      <@wcm.renderSlot id="SlotD" editableSlot="true"/>
                      <div class="container-fluid padding-top">
                        <div class="col-md-4 col-sm-6">
                          <div class="box"
												style="background-image: url(&#39;http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTIzOCZ2ZXI9MTAwMCZmaWxlPWdlc3Rhb3Blc3NvYXMrJTI4NjQweDQyNyUyOS5qcGcmY3JjPTM2ODk0MjUwOTUmc2l6ZT0wLjExNTIxMSZ1SWQ9MjU2JmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.jpg&#39;);"> <a href="http://bancorbras.fluig.com/portal/p/1/gestaopessoas" 
													class="title">
                            <div class="title-g">Mais Informações</div>
                            </a>
                            <div class="legend-black"> <font face="verdana"> <b>GESTÃO DE PESSOAS</b> </font> </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                          <div class="box"
												style="background-image: url(&#39;http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI3MCZ2ZXI9MTAwMCZmaWxlPXN1cHJpbWVudG9sb2dpc3RpY2ErJTI4NjQweDQyNyUyOS5qcGcmY3JjPTMzNzI5Njk1NTcmc2l6ZT0wLjEwMzQyMyZ1SWQ9MjU2JmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.jpg&#39;);"> <a href="http://bancorbras.fluig.com/portal/p/1/suprimentolog" 
													class="title">
                            <div class="title-g">Mais Informações</div>
                            </a>
                            <div class="legend-black"> <font face="verdana"> <b>SUPRIMENTO E LOGÍSTICA</b> </font> </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6">
                          <div class="box"
												style="background-image: url(&#39;http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI2OSZ2ZXI9MTAwMCZmaWxlPXN1cG9ydGV0ZWNub2xvZ2ljbyslMjg2NDB4NDI3JTI5LmpwZyZjcmM9ODI4MDgxMTkwJnNpemU9MC4xMjQ0MzcmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg&#39;);"> <a href="http://bancorbras.fluig.com/portal/p/1/suportetec"  class="title">
                            <div class="title-g">Mais Informações</div>
                            </a>
                            <div class="legend-black"> <font face="verdana"> <b>SUPORTE TECNOLÓGICO</b> </font> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="editable-slot slotfull layout-1-1" id="slotFull5">
                      <@wcm.renderSlot id="SlotEComunidade" editableSlot="true"/>
                     
                            <div class="row"> 
                              
                              <!-- Logo Centro -->
                              <div class="col-md-12"> <a href="#"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTIzMyZ2ZXI9MTAwMCZmaWxlPWNvbXVuaWRhZGUrYmFubmVyLmpwZyZjcmM9MTk4MTYyMTcwNCZzaXplPTAuMTk5OTE0JnVJZD0yNTYmZlNJZD0xJnVTSWQ9MSZkPWZhbHNlJnRrbj0mcHVibGljVXJsPXRydWU=.jpg" class="img-fluid" alt="Responsive image"> </a> </div>
                            </div>
                     
                      <!--  div class="containercom"> <img src="http://bancorbras.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0xMzg2JnZlcj0xMDAwJmZpbGU9Y29tdW5pZGFkZS5qcGcmY3JjPTM1NTg3NjkxNzgmc2l6ZT0wLjMyNzYwOCZ1SWQ9MjAmZlNJZD0xJnVTSWQ9MSZkPWZhbHNlJnRrbj0mcHVibGljVXJsPXRydWU=.jpg" alt="Snow" style="width:99%;">
                        <div class="bottom-leftcom"></div>
                        <div class="top-leftcom"></div>
                        <div class="top-rightcom"></div>
                        <div class="bottom-rightcom"></div>
                        <div class="centeredcom"><strong>
                          <h1>Comunidade CSC no Fluig</h1>
                          </strong>
                          <p>Timeline exclusiva do Centro de Serviços Compartilhados.</p>
                          <br>
                          <a href="http://bancorbras.fluig.com/portal/p/1/subject/dev-bancorbras" target="_blank">
                          <button class="btn btn-info btn-lg" id="botaocomunidade" name="botaocomunidade">Mais Informações</button>
                          </a> </div>
                      </div -->
                      
                      
                      
                      
                    </div>
                    
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="editable-slot slotfull layout-1-1" id="slotFull6">
                      <@wcm.renderSlot id="SlotFFooter" editableSlot="true"/>
                  
                  
                  
                  
                    </div>
                  </div>
                </div>
              </div>
              <!-- Div Fluig style --> 
            </div>
            <!-- Div Visualizacao pagina --> 
            
          </div>
        </div>
      </div>
    </div> 
  