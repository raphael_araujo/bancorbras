
<#import "/wcm.ftl" as wcm/>
 <!--    <@wcm.header authenticated="true"/>   --> 
    
    <!-- WCM Wrapper content -->
    <div class="wcm-wrapper-content1">
  <!--  <@wcm.menu />  -->
    
    <!-- Wrapper -->
    <div class="wcm-all-content1">
      <div id="wcm-content" class="clearfix wcm-background"> 
        
        <!-- Onde deverá estar a barra de formatação --> 
        <#if pageRender.isEditMode()=true>
        <div name="formatBar" id="formatBar"></div>
        <!-- Div geral --> 
        <!-- Há CSS distinto para Edição/Visualização -->
        <div id="edicaoPagina" class="clearfix"> <#else>
          <div id="visualizacaoPagina" class="clearfix"> </#if>
            <div class="fluig-style-guide"> 
              <!-- Titulo da página -->
           
              
              <!-- Slot 1 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull1">
                <@wcm.renderSlot id="SlotA" editableSlot="true"/>
                
                <!-- Inicio Header -->
                <div class="page-header">
                  <header class="main-header clearfix">
                    <div class="wrapper">
                      <div class="row"> 
                        
                        <!-- Logo Centro -->
                        <div class="col-md-12"> <img src="http://bancorbras.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTMxMCZ2ZXI9MTAwMCZmaWxlPWhlYWQuanBnJmNyYz0yOTg3NDk5MjI0JnNpemU9MC4xNjgwNzEmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg" class="img-fluid" alt="Responsive image"> </div>
                      </div>
                    </div>
                  </header>
                </div>
                <!-- Fim Header --> 
                
              </div>
              
              <!-- Slot 2 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull2">
                <@wcm.renderSlot id="SlotNavBar" editableSlot="true"/>
           
           
           
              </div>
              
              <!-- Slot 3 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull3">
                <@wcm.renderSlot id="SlotBanner" editableSlot="true"/>
                <div class="row"> 
                  
                  <!-- Logo Centro -->
                  <div class="col-md-12"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI5NyZ2ZXI9MTAwMCZmaWxlPWdlc3Rhb3Blc3NvYXNwYWdlLmpwZyZjcmM9OTYxNTE1NjQ4JnNpemU9MC4yNDU0NjEmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg" class="img-fluid" alt="Responsive image"> </div>
                </div>
              </div>
              
              <!-- Slot 4 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull4">
                <@wcm.renderSlot id="SlotD" editableSlot="true"/>
                
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel panel-info">
                        <div class="panel-heading text-center" style="background-color: #88cce1">
                          <h3 class="panel-title"> <font size="+1" style="color: #FFFFFF"> <strong>Confira a seguir os serviços que podem ser solicitados através de cada ferramenta do CSC.</strong> </font> </h3>
                        </div>
                        <div class="panel-body" style="background-color: #effafd">
                          <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Pesquisar" />
                        </div>
                        <table class="table" id="dev-table">
                          <thead>
                          </thead>
                          <tbody>
                            <!-- Linhas para editar opcoes tabela-->
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Adiantamento de diárias para viagem a serviço</strong></font></a>
                                <div id="collapse1" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Alteração de beneficiários do seguro de vida</strong></font></a>
                                <div id="collapse2" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                        
                              <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Análise da documentação das empresas tercerizadas contratadas</strong></font></a>
                                <div id="collapse3" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Atualização da CTPS</strong></font></a>
                                <div id="collapse4" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            
                            
                            
                                 <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Atualização de dados cadastrais, dependentes ou dados funcionais</strong></font></a>
                                <div id="collapse5" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Cadastramento de biometria</strong></font></a>
                                <div id="collapse6" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                        
                              <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse7"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Desligamento de colaborador</strong></font></a>
                                <div id="collapse7" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse8"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Efetivação da admissão do colaborador</strong></font></a>
                                <div id="collapse8" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            
                            
                                 <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse9"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Efetivação de férias</strong></font></a>
                                <div id="collapse9" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse10"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Emissão de carta de margem consignável</strong></font></a>
                                <div id="collapse10" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                        
                              <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse11"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Execução de rescisão complementar</strong></font></a>
                                <div id="collapse11" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse12"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Extrato do banco de horas</strong></font></a>
                                <div id="collapse12" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            
                            
                                 <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse13"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Homologação de rescisão do colaborador</strong></font></a>
                                <div id="collapse13" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse14"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Inclusão de crédito ou débito no banco de horas</strong></font></a>
                                <div id="collapse14" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                        
                              <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse15"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Inclusão ou exclusão da associação a Asfbrás</strong></font></a>
                                <div id="collapse15" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse16"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Inclusão ou exclusão de titular ou cônjuge no benefício de seguro de vida</strong></font></a>
                                <div id="collapse16" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            
                            
                                 <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse17"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Inclusão ou exclusão de titular ou dependente do benefício de seguro saúde</strong></font></a>
                                <div id="collapse17" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse18"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Inclusão, alteração ou exclusão do benefício de transporte</strong></font></a>
                                <div id="collapse18" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                        
                              <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse19"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Inclusão, alteração ou exclusão do benefício refeição/alimentação</strong></font></a>
                                <div id="collapse19" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse20"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Justificativa do ponto eletrônico</strong></font></a>
                                <div id="collapse20" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            
                                 <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse21"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Movimentação em folha de pagamento - auxílio-creche ou auxílio-educação</strong></font></a>
                                <div id="collapse21" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse22"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Movimentação em folha de pagamento - Demais eventos</strong></font></a>
                                <div id="collapse22" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                        
                              <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse23"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Prestação de contas de viagem a serviço</strong></font></a>
                                <div id="collapse23" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse24"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Programação de férias</strong></font></a>
                                <div id="collapse24" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            
                                 <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse25"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Saldo de abono assiduidade ou banco de horas</strong></font></a>
                                <div id="collapse25" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse26"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Segunda via de cartão refeição/alimentação</strong></font></a>
                                <div id="collapse26" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                        
                              <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse27"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Solicitação de abono assiduidade</strong></font></a>
                                <div id="collapse27" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                     
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse28"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Suspensão de contrato de trabalho</strong></font></a>
                                <div id="collapse28" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="https://portalcolaborador.bancorbras.com.br/" target="_blank">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            
                            
                            
                            
                            
                            
                            
                            <!-- Fim Linhas para editar opcoes tabela-->
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                
                
                
                
                
                
              </div>
              
              <!-- Slot 5 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull5">
                <@wcm.renderSlot id="SlotE" editableSlot="true"/>
          
          
              </div>
            </div>
          </div>
          <!-- FIM DAS WIDGETS DO LAYOUT -->
          <@wcm.footer layoutuserlabel="wcm.layoutdefaultpublic.user" />
        </div>
      </div>
    </div>