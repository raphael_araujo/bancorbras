 <#import "/wcm.ftl" as wcm/>
  <!--    <@wcm.header authenticated="true"/>   -->  
    
    <!-- WCM Wrapper content -->
    <div class="wcm-wrapper-content1">
<!--    <@wcm.menu />    -->
    
    <!-- Wrapper -->
    <div class="wcm-all-content1">
      <div id="wcm-content" class="clearfix wcm-background"> 
        
        <!-- Onde deverá estar a barra de formatação --> 
        <#if pageRender.isEditMode()=true>
        <div name="formatBar" id="formatBar"></div>
        <!-- Div geral --> 
        <!-- Há CSS distinto para Edição/Visualização -->
        <div id="edicaoPagina" class="clearfix"> <#else>
          <div id="visualizacaoPagina" class="clearfix"> </#if>
            <div class="fluig-style-guide"> 
              <!-- Titulo da página -->
           
              
              <!-- Slot 1 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull1">
                <@wcm.renderSlot id="SlotA" editableSlot="true"/>
                
                <!-- Inicio Header -->
                <div class="page-header">
                  <header class="main-header clearfix">
                    <div class="wrapper">
                      <div class="row"> 
                        
                        <!-- Logo Centro -->
                        <div class="col-md-12"> <img src="http://bancorbras.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTMxMCZ2ZXI9MTAwMCZmaWxlPWhlYWQuanBnJmNyYz0yOTg3NDk5MjI0JnNpemU9MC4xNjgwNzEmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg" class="img-fluid" alt="Responsive image"> </div>
                      </div>
                    </div>
                  </header>
                </div>
                <!-- Fim Header --> 
                
              </div>
              
              <!-- Slot 2 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull2">
                <@wcm.renderSlot id="SlotNavBar" editableSlot="true"/>
                
                
              
                
                
                
                
              </div>
              
              <!-- Slot 3 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull3">
                <@wcm.renderSlot id="SlotBanner" editableSlot="true"/>
                <div class="row"> 
                  
                  <!-- Logo Centro -->
                  <div class="col-md-12"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI5NSZ2ZXI9MTAwMCZmaWxlPWZpbmFuY2FzcGFnZS5qcGcmY3JjPTI0Mjg2MDYyNzgmc2l6ZT0wLjI0MDM3MiZ1SWQ9MjU2JmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.jpg" class="img-fluid" alt="Responsive image"> </div>
                </div>
              </div>
              
              <!-- Slot 4 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull4">
                <@wcm.renderSlot id="SlotD" editableSlot="true"/>
                
                <div class="container">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="panel panel-info">
                        <div class="panel-heading text-center" style="background-color: #88cce1">
                          <h3 class="panel-title"> <font size="+1" style="color: #FFFFFF"> <strong>Confira a seguir os serviços que podem ser solicitados através de cada ferramenta do CSC.</strong> </font> </h3>
                        </div>
                        <div class="panel-body" style="background-color: #effafd">
                          <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Pesquisar" />
                        </div>
                        <table class="table" id="dev-table">
                          <thead>
                          </thead>
                          <tbody>
                            <!-- Linhas para editar opcoes tabela-->
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Alteração e cancelamento de cheques emitidos</strong></font></a>
                                <div id="collapse1" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>

                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Alteração e cancelamento de cheques recebidos</strong></font></a><!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse2" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>

                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Análise e realização de pagamento</strong></font></a><!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse3" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Aplicação e resgate de investimentos</strong></font></a><!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse4" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Compra de moeda estrangeira</strong></font></a><!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse5" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Consulta de pagamento de cliente em conta bancária</strong></font></a><!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse6" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse7"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Depósito de cheques ou valores em espécie</strong></font></a><!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse7" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse8"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Emissão de comprovante de pagamento</strong></font></a><!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse8" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->

                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse9"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Recebimento de cheques ou valores em espécie</strong></font></a><!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse9" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse10"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Reembolso em espécie de pagamentos de despesas emergenciais</strong></font></a><!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse10" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                            <tr>
                              <td style="background-color: #effafd"><a data-toggle="collapse" data-parent="#accordion" href="#collapse11"><font size="+0.5" color="#1199c4"><strong><span class="fluigicon fluigicon-chevron-down fluigicon-xs"></span> Requisição de fundo fixo</strong></font></a><!-- Div Descricao Solicitacao -->
                                
                                <div id="collapse11" class="panel-collapse collapse">
                                  <div class="col-md-12" style="background-color: white">
                                    <div class="row"> 
                                      <!-- Descricao -->
                                      <div class="panel panel-default" id="descricao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>DESCRIÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto da Descricao -->
                                              <p>O serviço de cadastramento de nova biometria constitui em cadastrar a digital do colaborador no sistema de ponto eletrônico para registro de horário funcional. Deve ser solicitado quando há necessidade de novo registro biométrico.</p>
                                              <!-- Fim texto descricao --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- Forma Solicitacao -->
                                      <div class="panel panel-default col-lg-6" id="formaSolicitacaoo" style="height: 290px; border: hidden">
                                        <div class="panel-heading">
                                          <h3 class="panel-title"> <strong>FORMA DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p><strong>PROGRAMADO</strong><br>
                                                Os serviços programados são iniciados automaticamente pelo próprio fornecedor <br>
                                                <br>
                                                <strong>SOB DEMANDA</strong><br>
                                                Os serviços sob demanda são iniciados mediante
                                                solicitação do cliente. </p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      
                                      <!-- CANAIS DE SOLICITAÇAO -->
                                      <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="form-group col-md-12 text-info"> 
                                            <!-- Texto  -->
                                            <div class="bs-example">
                                              <table id="tabelaCanais" class="table table-hover">
                                                <tbody>
                                                  <tr>
                                                    <td class="col-lg-3 text-info"><strong>Sistema</strong></td>
                                                    <td class="text-info">Nome do Sistema:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>E-mail</strong></td>
                                                    <td class="text-info">Endereço:</td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Telefone</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Whatsapp</strong></td>
                                                    <td class="text-info">(061) </td>
                                                  </tr>
                                                  <tr>
                                                    <td class="text-info"><strong>Pessolmente</strong></td>
                                                    <td class="text-info">Local:</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </div>
                                            <!-- Fim texto  --> 
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- QUEM PODE SOLICITAR -->
                                      <div class="panel panel-default col-lg-6" id="podeSolicitar" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>QUEM PODE SOLICITAR</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto  -->
                                              <p>Qualquer colaborador das Empresas Bancorbrás.</p>
                                              <!-- Fim texto --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-lg-6" id="pontosAtencao" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"><strong>PONTOS DE ATENÇÃO</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>Não se aplica.</p>
                                              <!-- Fim  --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row"> 
                                      <!-- ACORDOS DE ENTREGA -->
                                      <div class="panel panel-default col-md-12" id="acordosEntrega" style="border: hidden">
                                        <div class="panel-heading ">
                                          <h3 class="panel-title"> <strong>ACORDOS DE ENTREGA</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                          <div class="row">
                                            <div class="form-group col-md-12 text-info"> 
                                              <!-- Texto -->
                                              <p>24 horas.</p>
                                              <!-- Fim --> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                     <div class="row">
                                      <div class="col-sm"></div>
                                      <div class="col-sm">
                                       <a href="#">
                                        <button type="button" class="btn btn-default btn-lg btn-block btn-info">SOLICITAR</button>
                                        </a>
                                      </div>
                                      <div class="col-sm"></div>
                                    </div>
                                  </div>
                              </div></td>
                            </tr>
                        
                 
                            
                            <!-- Fim Linhas para editar opcoes tabela-->
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                
                
                
                
                
              </div>
              
              <!-- Slot 5 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull5">
                <@wcm.renderSlot id="SlotE" editableSlot="true"/>
          
          
              </div>
            </div>
          </div>
          <!-- FIM DAS WIDGETS DO LAYOUT -->
          <@wcm.footer layoutuserlabel="wcm.layoutdefaultpublic.user" />
        </div>
      </div>
    </div>
