
      <#import "/wcm.ftl" as wcm/>
  <!--    <@wcm.header authenticated="true"/>  -->
    
    <!-- WCM Wrapper content -->
    <div class="wcm-wrapper-content1">
    <!--  <@wcm.menu /> -->
    
    <!-- Wrapper -->
    <div class="wcm-all-content1" style="padding: [0px, 0px, 0px, 0px];">
      <div id="wcm-content" class="clearfix wcm-background"> 
        
        <!-- Onde deverá estar a barra de formatação --> 
        <#if pageRender.isEditMode()=true>
        <div name="formatBar" id="formatBar"></div>
        <!-- Div geral --> 
        <!-- Há CSS distinto para Edição/Visualização -->
        <div id="edicaoPagina" class="clearfix"> <#else>
          <div id="visualizacaoPagina" class="clearfix"> </#if>
            <div class="fluig-style-guide"> 
              <!-- Titulo da página -->
           
              
              <!-- Slot 1 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull1">
                <@wcm.renderSlot id="SlotA" editableSlot="true"/>
                
                <!-- Inicio Header -->
                <div class="page-header">
                  <header class="main-header clearfix">
                    <div class="wrapper">
                      <div class="row"> 
                        
                        <!-- Logo Centro -->
                        <div class="col-md-12"> <img src="http://bancorbras.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTMxMCZ2ZXI9MTAwMCZmaWxlPWhlYWQuanBnJmNyYz0yOTg3NDk5MjI0JnNpemU9MC4xNjgwNzEmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg" class="img-fluid" alt="Responsive image"> </div>
                      </div>
                    </div>
                  </header>
                </div>
                <!-- Fim Header --> 
                
              </div>
              
              <!-- Slot 2 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull2">
                <@wcm.renderSlot id="SlotNavBar" editableSlot="true"/>
                






              </div>
              
    
              
              <!-- Slot 3 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull4">
                <@wcm.renderSlot id="SlotDTextos" editableSlot="true"/>
                
           
       <div class="row"> 
                  
                  <!-- Logo Centro -->
                  <div class="col-md-12"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI5OSZ2ZXI9MTAwMCZmaWxlPVF1ZW0rU29tb3MuanBnJmNyYz0xNDUzOTc5OTk5JnNpemU9MC4zMDMzMjYmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg" class="img-fluid" alt="Responsive image"> </div>
                </div>
    <div class="col-md-12" style="background-color: white">
      <div class="row"> 
        <!-- Descricao -->
        <div class="panel panel-default col-md-4" id="nossaMissao" style="border: hidden; height: 250px">
          <div class="panel-heading ">
            <h3 class="panel-title"><strong>NOSSA MISSÃO</strong></h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="form-group col-md-12 text-info"> 
                <!-- Texto da Descricao -->
                <font size="+1">Proporcionar a melhor experiência na entrega de serviços, suportando e alavancando os negócios do grupo com eficiência, eficácia, qualidade e custos menores.</font>
                <!-- Fim texto descricao --> 
              </div>
            </div>
          </div>
        </div>
        
        <div class="panel panel-default col-md-4" id="nossaVisao" style="border: hidden; height: 250px">
          <div class="panel-heading">
            <h3 class="panel-title"> <strong>NOSSA VISÃO</strong></h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="form-group col-md-12 text-info"> 
                <!-- Texto -->
                <font size="+1">Ser um centro integrado de excelência na entrega de serviços que suportam com efetividade o crescimento dos negócios do grupo.
                 
                </font>
                <!-- Fim --> 
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default col-md-4" id="nossosValores" style="border: hidden; height: 250px">
          <div class="panel-heading">
            <h3 class="panel-title"><strong>NOSSOS VALORES</strong></h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="form-group col-md-12 text-info"> 
                <!-- Texto -->
                <font size="+1">Comprometimento, profissionalismo, transparência, compromisso, honestidade, respeito à diversidade, confidencialidade e segurança da informação, ética, responsabilidade socioambiental e atendimento às leis e normas.</font>
                <!-- Fim --> 
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row"> 
        <!-- Forma Solicitacao -->
        <div class="panel panel-default col-lg-6" id="videoYoutube" style="border: hidden">
<div class="panel-body">
            <div class="row">
              <div class="form-group col-md-12 text-info"> 
                <!-- Texto -->
                <iframe width="560" height="315" src="https://www.youtube.com/embed/3uLGe3qTpPg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <!-- Fim --> 
              </div>
         
            </div>
          </div>
        </div>
        
        <!-- CANAIS DE SOLICITAÇAO -->
        <div class="panel panel-default col-lg-6" id="canaisSolicitacao" style="border: hidden">
          <div class="panel-heading ">
            <h3 class="panel-title"><strong>CANAIS DE SOLICITAÇÃO</strong></h3>
          </div>
          <div class="panel-body">
            <div class="form-group col-md-12 text-info"> 
              <font size="+1">
              <!-- Texto  -->
   Parte integrante do Planejamento Estratégico, a concepção do Centro de Serviços Compartilhados (CSC) foi uma solicitação das Diretorias atuais, que inferiram a necessidade do grupo adentrar em um modelo de Governança Corporativa. Assumido pelo Escritório de Projetos, o projeto iniciou em outubro...<a href="#">Leia mais.</a>
              <!-- Fim texto  --> 
              </font>
            </div>
          </div>
        </div>
      </div>

    </div>
    
    
    
                
                
                
                
                
                
                
                
              </div>
              
              <!-- Slot 5 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull5">
                <@wcm.renderSlot id="SlotE" editableSlot="true"/>

<!--  Slot para o Rodape -->


              </div>
            </div>
          </div>
          <!-- FIM DAS WIDGETS DO LAYOUT -->
      
        </div>
      </div>
    </div> 