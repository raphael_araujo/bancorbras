 <#import "/wcm.ftl" as wcm/>
  <!--    <@wcm.header authenticated="true"/>   --> 
    
    <!-- WCM Wrapper content -->
    <div class="wcm-wrapper-content1">
    <!--  <@wcm.menu /> -->
    
    <!-- Wrapper -->
    <div class="wcm-all-content1" style="padding: [0px, 0px, 0px, 0px];">
      <div id="wcm-content" class="clearfix wcm-background"> 
        
        <!-- Onde deverá estar a barra de formatação --> 
        <#if pageRender.isEditMode()=true>
        <div name="formatBar" id="formatBar"></div>
        <!-- Div geral --> 
        <!-- Há CSS distinto para Edição/Visualização -->
        <div id="edicaoPagina" class="clearfix"> <#else>
          <div id="visualizacaoPagina" class="clearfix"> </#if>
            <div class="fluig-style-guide"> 
              <!-- Titulo da página -->
           
              
              <!-- Slot 1 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull1">
                <@wcm.renderSlot id="SlotA" editableSlot="true"/>
                
                <!-- Inicio Header -->
                <div class="page-header">
                  <header class="main-header clearfix">
                    <div class="wrapper">
                      <div class="row"> 
                        
                        <!-- Logo Centro -->
                        <div class="col-md-12"> <img src="http://bancorbras.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTMxMCZ2ZXI9MTAwMCZmaWxlPWhlYWQuanBnJmNyYz0yOTg3NDk5MjI0JnNpemU9MC4xNjgwNzEmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg" class="img-fluid" alt="Responsive image"> </div>
                      </div>
                    </div>
                  </header>
                </div>
                <!-- Fim Header --> 
                
              </div>
              
              <!-- Slot BARRA NAVEGACAO -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull2">
                <@wcm.renderSlot id="SlotNavBar" editableSlot="true"/>
                

    




              </div>
              
       <!-- Slot  -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull2">
                <@wcm.renderSlot id="SlotC" editableSlot="true"/>
                

    
      <div class="row"> 
                  
                  <!-- Logo Centro -->
                  <div class="col-md-12"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI4MSZ2ZXI9MTAwMCZmaWxlPUJhbm5lcmwtQ1NDLUZBUS5qcGcmY3JjPTIwODE3MjQ3OTEmc2l6ZT0wLjIxOTYwMSZ1SWQ9MjU2JmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.jpg" class="img-fluid" alt="Responsive image">
                   </div>
                </div>



              </div>
              
              <!-- Slot 3 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull4">
                <@wcm.renderSlot id="SlotD" editableSlot="true"/>
                
           
     
     
     
     

    <div class="col-md-12" style="background-color: white">
     
     <div class="row">
     	<div class="form-group col-md-12 text-center text-info"><font size="+1"><strong> Está com dúvida em algum procedimento?<br>
Acesse nosso repositório de "perguntas mais frequentes" e descubra como resolver! </strong></font><br></div> 
     </div>
     
      <div class="row"> 
        <!-- Descricao -->
        <div class="panel panel-default col-md-4" id="nossaMissao" style="border: hidden;">
         
          <div class="panel-body">
            <div class="row">
            
               <div class="col-md-12"> <a href="http://bancorbras.fluig.com/portal/p/1/sobrecsc" target="_blank"><img  src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI4MyZ2ZXI9MTAwMCZmaWxlPUZBUVBvcnRhbENTQy5qcGcmY3JjPTY0NzAwOTI4JnNpemU9MC4wOTU5NTImdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.jpg" class="img-fluid" alt="Responsive image"></a> </div>
            </div>
          </div>
        </div>
        
        <div class="panel panel-default col-md-4" id="nossaVisao" style="border: hidden;">
         
          <div class="panel-body">
            <div class="row">
              
               <div class="col-md-12"><a href="http://bancorbras.fluig.com/portal/p/1/sobreservicos" target="_blank"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI4NCZ2ZXI9MTAwMCZmaWxlPUZBUVNvYnJlU2VydmklQzMlQTdvcy5qcGcmY3JjPTEyNjI5Mzk1Njcmc2l6ZT0wLjEyOTI1MiZ1SWQ9MjU2JmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.jpg" class="img-fluid" alt="Responsive image"></a> </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default col-md-4" id="nossosValores" style="border: hidden;">
         
          <div class="panel-body">
            <div class="row">
             
               <div class="col-md-12"><a href="http://bancorbras.fluig.com/portal/p/1/sobresla" target="_blank"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI4NSZ2ZXI9MTAwMCZmaWxlPUZBUVNvYnJlU0xBLmpwZyZjcmM9MzY0ODc0NDU0OSZzaXplPTAuMDc4ODI4JnVJZD0yNTYmZlNJZD0xJnVTSWQ9MSZkPWZhbHNlJnRrbj0mcHVibGljVXJsPXRydWU=.jpg" class="img-fluid" alt="Responsive image"></a> </div>
            </div>
          </div>
        </div>
      </div>
</div>
    
      
      
      
     
</div>
      
      
      
</div>
    
    
                
                
                
                
                
                
                
                
              </div>
              
              <!-- Slot 5 -->
              <div class="editable-slot slotfull layout-1-1" id="slotFull5">
                <@wcm.renderSlot id="SlotE" editableSlot="true"/>

<!--  Slot para o Rodape -->


              </div>
            </div>
          </div>
          <!-- FIM DAS WIDGETS DO LAYOUT -->
      
        </div>
      </div>
    </div> 