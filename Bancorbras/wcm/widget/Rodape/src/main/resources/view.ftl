<div id="HelloWorld_${instanceId}" class="super-widget wcm-widget-class fluig-style-guide"
     data-params="HelloWorld.instance({message: 'Hello world'})">



    <div>
       
                       <div id="myFooter">
                  <div class="container">
                    <div class="row">
                      <div class="col-sm-3 col-lg-4">
                        <h5>Sobre nós</h5>
                        <ul>
                          <li>Nós estamos diariamente trabalhando para melhorar a
                            sua experiência em atendimento, simplificando as etapas
                            de solicitação, processamento e entrega dos serviços e
                            disponibilizando informações relativas à sua solicitação.
                            
                            ​Entre em contato conosco e nos conte sobre a sua
                            experiência. :) </li>
                        </ul>
                      </div>
                      <div class="col-lg-3">
                        <h5>Localização</h5>
                        <ul>
                          <li><a href="https://goo.gl/maps/EAmmcoW31SU2" target="_blank"> Núcleo de Gestão do CSC <br>
                            SCS Quadra 04 Bloco A nº 230 <br>
                            Edifício Israel Pinheiro <br>
                            Asa Sul - Brasília - DF <br>
                            </a> 
                            <a href="mailto:faleconosco@csc.com.br" target="_top">faleconosco@csc.com.br</a>
                             <br>
                             <a href="#"> csc.fluig.com.br </a><br>
                            <a href="https://www.bancorbras.com.br" target="_blank">www.bancorbras.com.br</a></li>
                        </ul>
                      </div>
                      <div class="col-lg-5">
                        <h5>Conecte-se</h5>
                        <ul>
                          <li>
                            <div class="form-group">
                              <input class="form-control" type="email" placeholder="Enter email">
                              <button class="btn btn-info col-lg-12" id="botao" name="botao">Enviar</button>
                            </div>
                          </li>
                          <li>
                            <div class="form-group col-md-12">
                              <div class="form-group col-md-12"></div>
                              <div class="form-group col-md-2"> <a href="https://twitter.com/Bancorbras" target="_blank" class="twitter"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI0MSZ2ZXI9MTAwMCZmaWxlPXR3aXR0ZXJJY29uLnBuZyZjcmM9MTY2MjYwMTY2MSZzaXplPTcuODhFLTQmdUlkPTI1NiZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.png"> </a> </div>
                              <div class="form-group col-md-2"> <a href="https://www.facebook.com/bancorbras" target="_blank" class="facebook"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTIzNyZ2ZXI9MTAwMCZmaWxlPWZhY2Vib29rSWNvbi5wbmcmY3JjPTI2MzU5OTI5MTQmc2l6ZT02LjA3RS00JnVJZD0yNTYmZlNJZD0xJnVTSWQ9MSZkPWZhbHNlJnRrbj0mcHVibGljVXJsPXRydWU=.png"> </a> </div>
                              <div class="form-group col-md-2"> <a href="https://www.instagram.com/bancorbras/" target="_blank" class="instagram"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI0MCZ2ZXI9MTAwMCZmaWxlPUluc3RhZ3JhbUljb24ucG5nJmNyYz05MDA4MTk4Njgmc2l6ZT03LjgxRS00JnVJZD0yNTYmZlNJZD0xJnVTSWQ9MSZkPWZhbHNlJnRrbj0mcHVibGljVXJsPXRydWU=.png"> </a> </div>
                              <div class="form-group col-md-2"> <a href="https://www.youtube.com/user/CanalBancorbras" target="_blank" class="youtube"> <img src="http://bancorbras.fluig.com:80/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0zOTI0MiZ2ZXI9MTAwMCZmaWxlPVlvdXR1YmVJY29uLnBuZyZjcmM9MTUwNzAxMjMzJnNpemU9OC40NUUtNCZ1SWQ9MjU2JmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.png"> </a> </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="footer-copyright">
                    <p>© 2018 Copyright</p>
                  </div>
                </div>
       
       
       
       
       
       
       
    </div>


</div>
