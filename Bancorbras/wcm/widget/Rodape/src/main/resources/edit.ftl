<div id="HelloWorld_${instanceId}" class="super-widget wcm-widget-class fluig-style-guide"
     data-params="HelloWorld.instance({message: 'Hello world'})">

    <!-- efetua a tradução do texto do objeto i18n -->	
    <h1>EXEMPLO DO RODAPÉ (As modificaçoes feitas no Código aparecerão ao exportar.)</h1>
    
            <div id="myFooter">
                  <div class="container">
                    <div class="row">
                      <div class="col-sm-3 col-lg-4">
                        <h5>Sobre nós</h5>
                        <ul>
                          <li>Nós estamos diariamente trabalhando para melhorar a
                            sua experiência em atendimento, simplificando as etapas
                            de solicitação, processamento e entrega dos serviços e
                            disponibilizando informações relativas à sua solicitação.
                            
                            ​Entre em contato conosco e nos conte sobre a sua
                            experiência. :) </li>
                        </ul>
                      </div>
                      <div class="col-lg-3">
                        <h5>Localização</h5>
                        <ul>
                          <li><a href="https://goo.gl/maps/EAmmcoW31SU2" target="_blank"> Núcleo de Gestão do CSC <br>
                            SCS Quadra 04 Bloco A nº 230 <br>
                            Edifício Israel Pinheiro <br>
                            Asa Sul - Brasília - DF <br>
                            </a> faleconosco@csc.com.br <br>
                            csc.fluig.com.br </li>
                        </ul>
                      </div>
                      <div class="col-lg-5">
                        <h5>Conecte-se</h5>
                        <ul>
                          <li>
                            <div class="form-group">
                              <input class="form-control" type="email" placeholder="Enter email">
                              <button class="btn btn-info col-lg-12" id="botao" name="botao">Enviar</button>
                            </div>
                          </li>
                          <li>
                            <div class="form-group col-md-12">
                              <div class="form-group col-md-12"></div>
                              <div class="form-group col-md-2"> <a href="https://twitter.com/Bancorbras" class="twitter"> <img src="http://bancorbrastst.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0xODExJnZlcj0xMDAwJmZpbGU9dHdpdHRlckljb24ucG5nJmNyYz0xNjYyNjAxNjYxJnNpemU9Ny44OEUtNCZ1SWQ9MjAmZlNJZD0xJnVTSWQ9MSZkPWZhbHNlJnRrbj0mcHVibGljVXJsPXRydWU=.png"> </a> </div>
                              <div class="form-group col-md-2"> <a href="https://www.facebook.com/bancorbras" class="facebook"> <img src="http://bancorbrastst.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0xODA5JnZlcj0xMDAwJmZpbGU9ZmFjZWJvb2tJY29uLnBuZyZjcmM9MjYzNTk5MjkxNCZzaXplPTYuMDdFLTQmdUlkPTIwJmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.png"> </a> </div>
                              <div class="form-group col-md-2"> <a href="https://www.instagram.com/bancorbras/" class="instagram"> <img src="http://bancorbrastst.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0xODA4JnZlcj0xMDAwJmZpbGU9SW5zdGFncmFtSWNvbi5wbmcmY3JjPTkwMDgxOTg2OCZzaXplPTcuODFFLTQmdUlkPTIwJmZTSWQ9MSZ1U0lkPTEmZD1mYWxzZSZ0a249JnB1YmxpY1VybD10cnVl.png"> </a> </div>
                              <div class="form-group col-md-2"> <a href="https://www.youtube.com/user/CanalBancorbras" class="youtube"> <img src="http://bancorbrastst.fluig.com/volume/stream/Rmx1aWc=/P3Q9MSZ2b2w9RGVmYXVsdCZpZD0xODEwJnZlcj0xMDAwJmZpbGU9WW91dHViZUljb24ucG5nJmNyYz0xNTA3MDEyMzMmc2l6ZT04LjQ1RS00JnVJZD0yMCZmU0lkPTEmdVNJZD0xJmQ9ZmFsc2UmdGtuPSZwdWJsaWNVcmw9dHJ1ZQ==.png"> </a> </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="footer-copyright">
                    <p>© 2018 Copyright</p>
                  </div>
                </div>

</div>
