<div id="HelloWorld_${instanceId}" class="super-widget wcm-widget-class fluig-style-guide"
     data-params="HelloWorld.instance({message: 'Hello world'})" style="width: 100.45%;">

    <div>
    
    	<!-- Brand and toggle get grouped for better mobile display --> 
                
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="navbar navbar-expand justify-content-md-center bg-info">
                  <ul class="nav navbar-nav">
                    <li> <a href="http://bancorbras.fluig.com/wcmportalcsc"><font size="+0.5" ><strong>Início</strong></font></a> </li>
                    <li> <a href="http://bancorbras.fluig.com/quemsomos"><font size="+0.5" ><strong>Quem Somos</strong></font></a> </li>
                    <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><font size="+0.5" ><strong>Catálogo
                      de Serviços</strong></font> </a>
                      <ul class="dropdown-menu" role="menu">
                        <li> <a href="http://bancorbras.fluig.com/portal/p/1/assessoriajuridica"><font size="+0.5">Assessoria Jurídica</font></a> </li>
                        <li> <a href="http://bancorbras.fluig.com/portal/p/1/contabilidade"><font size="+0.5">Contabilidade</font></a> </li>
                        <li> <a href="http://bancorbras.fluig.com/portal/p/1/financas"><font size="+0.5">Finanças</font></a> </li>
                        <li> <a href="http://bancorbras.fluig.com/portal/p/1/gestaopessoas"><font size="+0.5">Gestão de Pessoas</font></a> </li>
                        <li> <a href="http://bancorbras.fluig.com/portal/p/1/suprimentolog"><font size="+0.5">Suprimento e Logística</font></a> </li>
                        <li> <a href="http://bancorbras.fluig.com/portal/p/1/suportetec"><font size="+0.5">Suporte Tecnológico</font></a> </li>
                      </ul>
                    </li>
                    <li> <a href="http://bancorbras.fluig.com/portal/p/1/paginafaq"><font size="+0.5" ><strong>FAQ</strong></font></a> </li>
                    <li> <a href="http://bancorbras.fluig.com/portal/p/1/paginanoticias"><font size="+0.5" ><strong>Notícias</strong></font></a> </li>
                    <li> <a href="https://www.bancorbras.com.br/central-atendimento/contatos/"><font size="+0.5" ><strong>Contato</strong></font></a> </li>
                  </ul>
                </div>
    
    
    
    
    
    
    
    
    
    
        
    </div>

    

</div>
