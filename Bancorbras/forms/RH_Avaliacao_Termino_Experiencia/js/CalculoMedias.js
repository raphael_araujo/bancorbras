/*************************************************
 *  Media do ASSIMILAÇÃO DO TRABALHO E INTERESSE
 *************************************************/
 function MediaAssimi(){
var media, n1, n2, n3, n4 ;

n1= document.getElementById("AssimiFacilidade").value;

n1= eval (n1) ;

n2= document.getElementById("AssimiInteresse").value;
n2= eval (n2) ; 

n3= document.getElementById("AssimiApresentaPrest").value; 
n3= eval (n3) ;

n4= document.getElementById("AssimiApresenSugest").value; 
n4= eval (n4) ;

media = (n1 + n2 + n3 + n4)/4 ; 


if ( media > 3 ) 
{

document.getElementById("VlrmediaAssimi").value = media;
    
document.getElementById("mediaAssimi").value = "Supera";
  

} 
if ( media <= 3 && media > 2) 
{
document.getElementById("VlrmediaAssimi").value = media;
    
document.getElementById("mediaAssimi").value = "Atende";
    

} 
if ( media <= 2 && media > 1) 
{
document.getElementById("VlrmediaAssimi").value = media;
    
document.getElementById("mediaAssimi").value = "Em Desenvolvimento";
    

} 

if ( media <= 1 ) 
{

document.getElementById("VlrmediaAssimi").value = media;
    
document.getElementById("mediaAssimi").value = "Não Atende";
    

}

}
 
 
 /*************************************************
  *** QUALIDADE DO TRABALHO PRODUZIDO *************
  *************************************************/
 function MediaQuali(){
	 var media, n1, n2, n3, n4 ;

	 n1= document.getElementById("QualiRealizaAtivi").value;

	 n1= eval (n1) ;

	 n2= document.getElementById("QualiAtendePadroes").value;
	 n2= eval (n2) ; 

	 n3= document.getElementById("QualiCumprePrazos").value; 
	 n3= eval (n3) ;

	 n4= document.getElementById("QualiAtingeNProdu").value; 
	 n4= eval (n4) ;

	 media = (n1 + n2 + n3 + n4)/4 ; 

	 
	 if ( media > 3 ) 
	 {

	 document.getElementById("VlrmediaQuali").value = media;
	     
	 document.getElementById("mediaQuali").value = "Supera";
	   

	 } 
	 if ( media <= 3 && media > 2) 
	 {
	 document.getElementById("VlrmediaQuali").value = media;
	     
	 document.getElementById("mediaQuali").value = "Atende";
	     

	 } 
	 if ( media <= 2 && media > 1) 
	 {
	 document.getElementById("VlrmediaQuali").value = media;
	     
	 document.getElementById("mediaQuali").value = "Em Desenvolvimento";
	     

	 } 

	 if ( media <= 1 ) 
	 {

	 document.getElementById("VlrmediaQuali").value = media;
	     
	 document.getElementById("mediaQuali").value = "Não Atende";
	     

	 }

	 }
 /*************************************************
  *************** INICIATIVA **********************
  *************************************************/
 
 
 function MediaInici(){
	 var media, n1, n2, n3, n4 ;

	 n1= document.getElementById("IniciDemontraInici").value;

	 n1= eval (n1) ;

	 n2= document.getElementById("IniciRecorreLider").value;
	 n2= eval (n2) ; 

	 n3= document.getElementById("IniciOfereceAlt").value; 
	 n3= eval (n3) ;

	 n4= document.getElementById("IniciTomaRespon").value; 
	 n4= eval (n4) ;

	 media = (n1 + n2 + n3 + n4)/4 ; 

	 
	 if ( media > 3 ) 
	 {

	 document.getElementById("VlrmediaInici").value = media;
	     
	 document.getElementById("mediaInici").value = "Supera";
	   

	 } 
	 if ( media <= 3 && media > 2) 
	 {
	 document.getElementById("VlrmediaInici").value = media;
	     
	 document.getElementById("mediaInici").value = "Atende";
	     

	 } 
	 if ( media <= 2 && media > 1) 
	 {
	 document.getElementById("VlrmediaInici").value = media;
	     
	 document.getElementById("mediaInici").value = "Em Desenvolvimento";
	     

	 } 

	 if ( media <= 1 ) 
	 {

	 document.getElementById("VlrmediaInici").value = media;
	     
	 document.getElementById("mediaInici").value = "Não Atende";
	     

	 }

	 }
 
 
 /*************************************************
  *************** RELACIONAMENTO INTERPESSOAL *****
  *************************************************/
 
 
 function MediaRelaInt(){
	 var media, n1, n2, n3, n4 ;

	 n1= document.getElementById("RelaIntRespSuperior").value;

	 n1= eval (n1) ;

	 n2= document.getElementById("RelaIntRespIntEquipes").value;
	 n2= eval (n2) ; 

	 n3= document.getElementById("RelaIntRelaColegas").value; 
	 n3= eval (n3) ;

	 n4= document.getElementById("RelaIntRelaPessoasAmb").value; 
	 n4= eval (n4) ;

	 media = (n1 + n2 + n3 + n4)/4 ; 

	 
	 if ( media > 3 ) 
	 {

	 document.getElementById("VlrmediaRelaInt").value = media;
	     
	 document.getElementById("mediaRelaInt").value = "Supera";
	   

	 } 
	 if ( media <= 3 && media > 2) 
	 {
	 document.getElementById("VlrmediaRelaInt").value = media;
	     
	 document.getElementById("mediaRelaInt").value = "Atende";
	     

	 } 
	 if ( media <= 2 && media > 1) 
	 {
	 document.getElementById("VlrmediaRelaInt").value = media;
	     
	 document.getElementById("mediaRelaInt").value = "Em Desenvolvimento";
	     

	 } 

	 if ( media <= 1 ) 
	 {

	 document.getElementById("VlrmediaRelaInt").value = media;
	     
	 document.getElementById("mediaRelaInt").value = "Não Atende";
	     

	 }

	 }
 
 
 

 /*************************************************
  *************** DISCIPLINA **** *****************
  *************************************************/
 
 
 function MediaDisci(){
	 var media, n1, n2, n3, n4 ;

	 n1= document.getElementById("DisciCumpreJornadaTrabalho").value;

	 n1= eval (n1) ;

	 n2= document.getElementById("DisciAusentaFrequencia").value;
	 n2= eval (n2) ; 

	 n3= document.getElementById("DisciCumpreNormas").value; 
	 n3= eval (n3) ;

	 n4= document.getElementById("DisciPontual").value; 
	 n4= eval (n4) ;



	 
	 
	 media = (n1 + n2 + n3 + n4 )/4 ; 

	 
	 if ( media > 3 ) 
	 {

	 document.getElementById("VlrmediaDisci").value = media;
	     
	 document.getElementById("mediaDisci").value = "Supera";
	   

	 } 
	 if ( media <= 3 && media > 2) 
	 {
	 document.getElementById("VlrmediaDisci").value = media;
	     
	 document.getElementById("mediaDisci").value = "Atende";
	     

	 } 
	 if ( media <= 2 && media > 1) 
	 {
	 document.getElementById("VlrmediaDisci").value = media;
	     
	 document.getElementById("mediaDisci").value = "Em Desenvolvimento";
	     

	 } 

	 if ( media <= 1 ) 
	 {

	 document.getElementById("VlrmediaDisci").value = media;
	     
	 document.getElementById("mediaDisci").value = "Não Atende";
	     

	 }

	 }
 
 
 /*************************************************
  *************** COMPROMETIMENTO *****************
  *************************************************/
 
 
 function MediaComprome(){
	 var media, n1, n2, n3, n4 ;

	 n1= document.getElementById("CompromeEnvolveOrgan").value;

	 n1= eval (n1) ;

	 n2= document.getElementById("CompromeEticoResponsavel").value;
	 n2= eval (n2) ; 

	 n3= document.getElementById("CompromeInteressado").value; 
	 n3= eval (n3) ;

	 n4= document.getElementById("CompromeProfissional").value; 
	 n4= eval (n4) ;

	 media = (n1 + n2 + n3 + n4)/4 ; 

	 
	 if ( media > 3 ) 
	 {

	 document.getElementById("VlrmediaComprome").value = media;
	     
	 document.getElementById("mediaComprome").value = "Supera";
	   

	 } 
	 if ( media <= 3 && media > 2) 
	 {
	 document.getElementById("VlrmediaComprome").value = media;
	     
	 document.getElementById("mediaComprome").value = "Atende";
	     

	 } 
	 if ( media <= 2 && media > 1) 
	 {
	 document.getElementById("VlrmediaComprome").value = media;
	     
	 document.getElementById("mediaComprome").value = "Em Desenvolvimento";
	     

	 } 

	 if ( media <= 1 ) 
	 {

	 document.getElementById("VlrmediaComprome").value = media;
	     
	 document.getElementById("mediaComprome").value = "Não Atende";
	     

	 }

	 }
 
 
 
 /*************************************************
  *************** MEDIA TOTAL **** *****************
  *************************************************/
 
 
 function MediaResultado(){
	
	 var media, n1, n2, n3, n4, n5, n6 ;

	 n1= document.getElementById("VlrmediaAssimi").value;

	 n1= eval (n1) ;

	 n2= document.getElementById("VlrmediaQuali").value;
	 n2= eval (n2) ; 

	 n3= document.getElementById("VlrmediaInici").value; 
	 n3= eval (n3) ;

	 n4= document.getElementById("VlrmediaRelaInt").value; 
	 n4= eval (n4) ;


	 n5= document.getElementById("VlrmediaDisci").value; 
	 n5= eval (n5) ;
	 
	 
	 n6= document.getElementById("VlrmediaComprome").value; 
	 n6= eval (n6) ;
	 
	 
	 media = (n1 + n2 + n3 + n4 + n5 + n6 )/6 ; 

	 
	 if ( media > 3 ) 
	 {

	
	     
	 document.getElementById("Resultado").value = "Supera";
	   

	 } 
	 if ( media <= 3 && media > 2) 
	 {
	 
	     
	 document.getElementById("Resultado").value = "Atende";
	     

	 } 
	 if ( media <= 2 && media > 1) 
	 {
	
	     
	 document.getElementById("Resultado").value = "Em Desenvolvimento";
	     

	 } 

	 if ( media <= 1 ) 
	 {

	
	     
	 document.getElementById("Resultado").value = "Não Atende";
	     

	 }

	 }
 
 
 
 