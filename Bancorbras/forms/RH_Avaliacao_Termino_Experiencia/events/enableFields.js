function enableFields(form) {		
	var activity = getValue('WKNumState');
	
	if ( 
		 activity == 0 || activity == 4 || activity == 16 ) 
	{
		//Dados da Solicitacao
		form.setEnabled('AssimiFacilidade', false);
		form.setEnabled('AssimiInteresse', false);
		form.setEnabled('AssimiApresentaPrest', false);
		form.setEnabled('AssimiApresenSugest', false);
		
		
		form.setEnabled('QualiRealizaAtivi', false);
		form.setEnabled('QualiAtendePadroes', false);
		form.setEnabled('QualiCumprePrazos', false);
		form.setEnabled('QualiAtingeNProdu', false);
		
		
		
		form.setEnabled('IniciDemontraInici', false);
		form.setEnabled('IniciRecorreLider', false);

		form.setEnabled('IniciOfereceAlt', false);
		form.setEnabled('IniciTomaRespon', false);

		form.setEnabled('RelaIntRespSuperior', false);
		form.setEnabled('RelaIntRespIntEquipes', false);

		form.setEnabled('RelaIntRelaColegas', false);
		form.setEnabled('RelaIntRelaPessoasAmb', false);

		form.setEnabled('DisciCumpreJornadaTrabalho', false);
		form.setEnabled('DisciAusentaFrequencia', false);
		form.setEnabled('DisciCumpreNormas', false);
		form.setEnabled('DisciPontual', false);
		
		
		form.setEnabled('CompromeEnvolveOrgan', false);
		form.setEnabled('CompromeEticoResponsavel', false);
		form.setEnabled('CompromeInteressado', false);
		form.setEnabled('CompromeProfissional', false);
		
		
		
		form.setEnabled('observacoes', false);
		form.setEnabled('Efetivacao', false);
		
		
		form.setEnabled('observacoes', false);
		form.setEnabled('observacoes', false);
		
	}
	

	
	

}