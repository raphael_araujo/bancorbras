$( document ).ready(function() {
	activeZoons();
	
});


//pega matricula fluig pelo mail
function getUserByMail(mail){
	var constraints = new Array();
	var dataset = null;
	var user = {"colleagueName":"","colleagueId":""};

	constraints.push(DatasetFactory.createConstraint("mail", mail, mail, ConstraintType.MUST));
	dataset = DatasetFactory.getDataset("colleague", null, constraints, null);
	
	if(dataset.values.length > 0) {
		user.colleagueId = dataset.values[0]["colleaguePK.colleagueId"];
		user.colleagueName = dataset.values[0]["colleagueName"];
	}
	
	return user;
	
}


function getAprov(mail){
	var constraints = new Array();
	var dataset = null;
	var aprov = {"nome":"","email":"","departamento":""};
	
	constraints.push(DatasetFactory.createConstraint("mail", mail, mail, ConstraintType.MUST));
	
	dataset = DatasetFactory.getDataset("ds_aprovdepart", null, constraints, null);
	
	if (dataset == null) {
			
		alert("Erro na comunicação com o Protheus! Atutalize a pagina... Caso erro persista entre em contato com a TI!")
		
	}else{
		
		if(dataset.values.length > 0) {
			aprov.nome = dataset.values[0]["NOME"];
			aprov.departamento = dataset.values[0]["DEPARTAMENTO"];
			aprov.email = dataset.values[0]["EMAIL"];
		}
		else{
			
			alert("Dados do aprovador não localizado. Procure o DP!")
			
		}	
		
	}
	

	
	return aprov;
}

