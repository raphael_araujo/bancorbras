/**
 * Id dos botoes de zoom sem prefixo.
 */
var ZOOM_PRODUTO = 'A5___';
var ZOOM_FILIAL_SOL = 'A14';
var ZOOM_FILIAL_ENT = 'A15';
var ZOOM_GRUPO = 'A50___';
var ZOOM_CCUSTO = 'AA1';
var ZOOM_APROVADOR = 'AA0';

var index = "";

/**
 * Habilita o evento click nos botoes de zoom.
 * 
 * @returns void.
 */
function activeZoons(){
	
	$('.btnZoom').click(function(){
		
		if ($('#A0').val() == "") {
			
			var zoom = getZoomParams(removePrefix(this.id),null);
			if(zoom == null) return;
			openZoom(zoom.title, zoom.datasetId, zoom.dataFields, zoom.resultFields, zoom.name, zoom.filterValues);		
			
		}
		
	});
}

/**
 * Habilita o evento blur nos campos de zoom.
 * 
 * @returns void.
 */
function activeQuickSearch(){
	
	
	$('.quickSearch').blur(function(){
		if(this.value == "") return;
		var id = removePrefix(this.id);
		var zoom = getZoomParams(id);
		var resultFields;		
		var c1;		
		var dataset;
		
		if(zoom == null) this.value = '';
		
		resultFields = zoom.resultFields.split(',');		
		c1 = DatasetFactory.createConstraint(zoom.PK, this.value, this.value, ConstraintType.MUST);		
		dataset = DatasetFactory.getDataset(zoom.datasetId, resultFields, [c1]);
		
		if(dataset.values.length == 1){
			dataset = dataset.values[0];
			dataset.type = id;
			setSelectedZoomItem(dataset);		
		}
		else if(dataset.values.length > 1){
			alert('Há mais de um registro para o código '+this.value);
			this.value = '';
		}
		else{
			alert('Não foi encontrado registro para o código '+this.value);
			this.value = '';
		}
	});
}

/**
 * Retorna os parametros do zoom.
 * Atributo PK eh usado no quickSearch.
 * 
 * @param id: id do botao de zoom.
 * @returns {Zoom}.
 */
function getZoomParams(id,indice){
	zoom = {"PK":"", "title":"", "datasetId":"", "dataFields":"", "resultFields":"", "name":"", "filterValues":""};
	zoom.name = id;
	if(indice != null){
			index=indice;
	}
	switch(id){
		case ZOOM_GRUPO + indice:
			zoom.PK = 'CODIGO';
			zoom.title = 'Grupo';
			zoom.datasetId = 'ds_grupo';
			zoom.dataFields = 'CODIGO,'+escape("C&oacute;digo")+',DESCRICAO,'+escape("Descri&ccedil;&atilde;o")+'';
			zoom.resultFields = 'CODIGO';
			break;
		case ZOOM_CCUSTO:
			zoom.PK = 'CODIGO';
			zoom.title = 'Grupo';
			zoom.datasetId = 'ds_ccusto';
			zoom.dataFields = 'CODIGO,'+escape("C&oacute;digo")+',DESCRICAO,'+escape("Descri&ccedil;&atilde;o")+'';
			zoom.resultFields = 'CODIGO,DESCRICAO';
			break;
		case ZOOM_PRODUTO + indice:
			var grupo = $("#A50___" + indice).val();
			zoom.PK = 'CODIGO';
			zoom.title = 'Grupo';
			zoom.datasetId = 'ds_produto';
			zoom.dataFields = 'CODIGO,'+escape('C&oacute;digo')+',DESCRICAO,'+escape('Descri&ccedil;&atilde;o')+',UNIDADE,Unidade,CCUSTO,Centro de Custo,ARMAZEM,'+escape('Armaz&eacute;m');
			zoom.resultFields = 'CODIGO,DESCRICAO,UNIDADE,CCUSTO,ARMAZEM';
			if(grupo)
			{
				zoom.filterValues="grupo," + grupo;
			}
			break;
		case ZOOM_FILIAL_SOL:
			zoom.PK = 'CODIGO';
			zoom.title = 'Consulta de Filial';
			zoom.datasetId = 'ds_filial';
			zoom.dataFields = 'CODIGO,'+escape('C&oacute;digo')+',DESCRICAO,'+escape('Descri&ccedil;&atilde;o')+'';
			zoom.resultFields = 'CODIGO,DESCRICAO';
			break;
		case ZOOM_FILIAL_ENT:
			zoom.PK = 'CODIGO';
			zoom.title = 'Consulta de Filial';
			zoom.datasetId = 'ds_filial';
			zoom.dataFields = 'CODIGO,'+escape('C&oacute;digo')+',DESCRICAO,'+escape('Descri&ccedil;&atilde;o')+'';
			zoom.resultFields = 'CODIGO,DESCRICAO';
			break;
		case ZOOM_APROVADOR:
			var mail = $('#MAIL_SOL').val();
			//zoom.PK = 'CODIGO';
			zoom.title = 'Consulta Aprovador';
			zoom.datasetId = 'ds_aprovdepart';
			zoom.dataFields = 'NOME,'+escape('Aprovador')+',DEPARTAMENTO,'+escape('Departamento')+',EMAIL,'+escape('E-mail')+'';
			zoom.resultFields = 'NOME,DEPARTAMENTO,EMAIL';
			zoom.filterValues="mail," + mail;
			break;	
		default:
			alert('Zoom '+id+' não está parametrizado!');
			break;
	}
	
	return zoom;
}

/**
 * Chamada padrao de zoom do Fluig.
 * 
 * @param title: Titulo da janela do Zoom.
 * @param datasetId: Id do dataset.
 * @param dataFields: Colunas a serem exibidas no zoom. 'coluna1,label1,coluna2,label2'.
 * @param resultFields: Colunas a serem retornadas pelo zoom. 'coluna1,coluna2'.
 * @param name: nome do type do zoom.
 * @param filterValues: Filtros. 'nomeFiltro,valorFiltro,nomeFiltro2,valorFiltro2';.
 * @returns void.
 */
function openZoom(title, datasetId, dataFields, resultFields, name, filterValues){
	var url = '/webdesk/zoom.jsp?';
	url+= 'title='+title+'&';
	url+= 'datasetId='+datasetId+'&';
	url+= 'dataFields='+dataFields+'&';
	url+= 'resultFields='+resultFields+'&';
	url+= 'type='+name+'&';
	url+= 'filterValues='+filterValues;

	window.open(url, 'zoom', 'status, scrollbars=no, width=800, height=350, top=0, left=0');
}

/**
 * Remove os prefixos dos ids dos elementos.
 * Esta funcao permite que o quickSearch tambem utilize a setSelectedZoomItem.
 * 
 * Para este projeto os prefixos seguem o seguinte padrao:
 * -id_ = codigo / identificador.
 * -txt_ = descricao.
 * -btn_ = botao. * 
 * 
 * @param id: id do elemento.
 * @returns {String}: id do elemento sem prefixo.
 */
function removePrefix(id){
	console.log("id:"+id);
	return id.replace('id_','').replace('txt_','').replace('nm_','').replace('btn_','');
}	

/**
 * Funcao de retorno dos dados selecionados no zoom.
 *
 * @param selectedItem: Dados retornados pelo zoom.
 * @return void.
 */
function setSelectedZoomItem(selectedItem){
	switch(selectedItem.type){
		case ZOOM_GRUPO:
			setProduto(selectedItem);
			break;
		case ZOOM_PRODUTO + index:
			setProduto(selectedItem);
			break;
		case ZOOM_FILIAL_SOL:
			setFilialSol(selectedItem);
			break;
		case ZOOM_FILIAL_ENT:
			setFilialEnt(selectedItem);
			break;
		case ZOOM_CCUSTO:
			setcusto(selectedItem);
			break;
		case ZOOM_APROVADOR:
			setaprov(selectedItem);
			break;						
		default:
			setValue(selectedItem.type, selectedItem.CODIGO)			
			break;
	}	
}

function setValue(fieldId, value){
	var field = $('#' + fieldId);
	
	if(field && value)
		field.val(value);
}

function throwIfNull(val, campo){
	if(val == null || val.length == 0) {
		throw "O Campo " + campo + " deve ser preenchido.";
	}
}


//Abaixo estao as funcoes de preenchimento dos campos com os valores retornados pelo zoom ou quickSearch.

function setProduto(selectedItem){
	
	$('#A5___'+index).val(selectedItem.CODIGO);
	$('#A7___'+index).val(selectedItem.DESCRICAO);
	$('#A9___'+index).val(selectedItem.ARMAZEM);
	$('#A6___'+index).val(selectedItem.UNIDADE);
}
function setFilialSol(selectedItem){
	$('#A14').val(selectedItem.CODIGO);
}
function setFilialEnt(selectedItem){
	$('#A15').val(selectedItem.CODIGO);
}
function setcusto(selectedItem){
	$('#AA1').val(selectedItem.CODIGO);
	$('#_AA1').val(selectedItem.DESCRICAO);
}
function setaprov(selectedItem){
	
	userflg = getUserByMail(selectedItem.EMAIL);	
	
	if (userflg.colleagueId == "") {
		
		alert("Aprovador não cadastrado no FLUIG! Entre em contato com o SERVICEDESK.");
		
		$('#_AA0').val("");
		$('#AA0').val("");
		$('#_AA2').val("");
		
	}else{
		
		$('#AA2').val(userflg.colleagueId);
		$('#_AA0').val(selectedItem.NOME);
		$('#AA0').val(selectedItem.EMAIL);
		$('#_AA2').val(selectedItem.DEPARTAMENTO);
		
	}
	
}









