/*******************************************************************************
 * • Número de dias: Campo de preenchimento automático, somente leitura, que irá
 * calcular a quantidade de dias de férias selecionado conforme período Descanso
 * de e Descanso até;
 * *******************************************************************************
 */

function calcularDatas() {

	var dat1 = document.getElementById("descansoDe").value;
	var date1 = new Date(dat1);

	var dat2 = document.getElementById("descansoAte").value;
	var date2 = new Date(dat2);

	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

	// Validar Data menor que 5 dias
	if (diffDays < 6) {
		alert("O período deve ser maior que 5 dias");
		document.getElementById('nrDias').value = "";

	} else {

		document.getElementById('nrDias').value = diffDays;
	}
	if (diffDays > 30) {
		alert("O período não pode ser maior que 30 dias");
		document.getElementById('nrDias').value = "";
	} else {

		document.getElementById('nrDias').value = diffDays;
	}
	
	
	// Validar Abono Pecuniario
	if (diffDays < 6) {
	
		document.getElementById("AbonoPecuniario").disabled = false;

	} 
	if (diffDays > 14) {
		document.getElementById("AbonoPecuniario").disabled = false;
		
	} 
	
	if (diffDays > 6 && diffDays < 14) {
		document.getElementById("AbonoPecuniario").disabled = true;
		
	} 
	
	

}

/*******************************************************************************
 * ************* FUNCAO QUE IRÁ CALCULAR A DATA DISPONIVEL PARA ***
 * ************************SOLICITAR AS FERIAS ********************
 * ****************************************************************
 */

// Funcao das datas
function addData() {

	// Pegar data Atual para somar
	var currentDate = new Date();

	// pegar data atual para exibir
	var currentDate1 = new Date();

	// adicionar Dias
	currentDate.setDate(currentDate.getDate() + 70);

	// Adicionar meses capturado
	// currentDate.setMonth(currentDate.getMonth()+a);

	// Trazer data Atual
	currentDate1.setDate(currentDate1.getDate());

	// alert("data "+currentDate.toLocaleDateString());

	// Exibir data Atual
	document.getElementById('dataatual').value = currentDate1
			.toLocaleDateString();

	// Exibir a data ja atualizada
	document.getElementById("dataDisponivel").innerHTML = currentDate
			.toLocaleDateString();

	var dateDisponviel = currentDate.toLocaleDateString();
	// alert("date Saida " +dateDisponviel);

	var newdateDisponviel = dateDisponviel.split("/").reverse().join("-");
	// alert("New date Saida "+newdateDisponviel);

	document.getElementById('descansoDe').value = newdateDisponviel;
	// document.getElementById('descansoAte').value = newdateDisponviel;
	document.getElementById("descansoDe")
			.setAttribute("min", newdateDisponviel);
}

/*******************************************************************************
 * ***FUNCAO DEFINIR DATA MINIMA A PARTIR DA DATA SELECIONADA******
 * ******************NO PERIODO DE INICIO**************************
 * ****************************************************************
 */

function minData() {

	var datamax = document.getElementById('descansoDe').value;

	var newdatesMax = datamax.split("/").reverse().join("-");
	// alert("New date Max "+newdatesMax);

	document.getElementById("descansoAte").setAttribute("min", newdatesMax);

}

/*******************************************************************************
 * *************EMPRESA DF DESBLOQUEAR CAMPO ACORDO COLETIVO******
 * ****************************************************************
 */

function validarCampoAcordo() {

	var UFempresa = document.getElementById("EMPREST_ACT").value;

	if (UFempresa == "SIM" || UFempresa == "sim") {

		document.getElementById("AdiantamentoAcordoCol").disabled = false;

	}

	if (UFempresa != "SIM" || UFempresa != "sim") {

		document.getElementById("AdiantamentoAcordoCol").disabled = true;

	}

}

/*******************************************************************************
 * ************* FUNCAO TESTE Datepicker*************************
 * ****************************************************************
 *
$(function() {
	console.log("1");
	$('#dataentrada').dateRangePicker(
			{

				autoClose : true,
				format : 'DD/MM/YYYY',
				separator : ' até ',
				minDays : 5,
				maxDays : 30,
				startDate : moment(),
				selectForward : true,
				language : 'pt',
				getValue : function() {
					if ($('#dataentrada').val() && $('#datasaida').val())
						return $('#dataentrada').val() + ' até '
								+ $('#datasaida').val();
					else
						return '';
				},
				setValue : function(s, s1, s2) {
					$('#dataentrada').val(s1);
					$('#datasaida').val(s2);
				}
			});
	/*
	 * $('#datasaida').dateRangePicker({ autoClose: true, format: 'DD/MM/YYYY',
	 * separator: ' até ', // minDays: 1, startDate: '05/06/2017',
	 * selectForward: true, getValue: function() { if ($('#dataentrada').val() &&
	 * $('#datasaida').val() ) return $('#dataentrada').val() + ' até ' +
	 * $('#datasaida').val(); else return ''; }, setValue: function(s,s1,s2) {
	 * $('#dataentrada').val(s1); $('#datasaida').val(s2); } });
	 */

//  });

/**********************************************************************************************************
 * Adiantamento de 1ª parcela de 13º salário? Se a data de início das férias for nos meses de fevereiro, 
 * março ou abril, permitir que o usuário solicite o adiantamento da 1ª parcela do 13º salário, 
 * somente disponível para edição nas atividades solicitar férias e Revisar Solicitação;
 **********************************************************************************************************/

function obtemMes() {
    var mes = document.getElementById("descansoDe").value.split("-")[1];
    document.getElementById("messelect").value = mes;
    
    if (mes == 02 || mes == 03 || mes == 04) {
    
    	document.getElementById("AdiantamentoParcela13").disabled = false;
    		
    }else{
    	
    	
    	document.getElementById("AdiantamentoParcela13").disabled = true;
    	
    }
    
    
}





