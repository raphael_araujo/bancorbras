function displayFields(form,customHTML){ 
	if(getValue("WKNumState") == 0){
/*******************************************
 * Zoom Aprovador - Aprovador 
 *******************************************/
	    //Pego dados do solicitante
	    user = getUser(getValue("WKUser"));
	    var nomuser = user.colleagueName;
	    nomuser = nomuser.toUpperCase();
	    form.setValue("A1", nomuser);
	    form.setValue("A1_MATRICULA", getValue("WKUser"));
	    form.setValue("MAIL_SOL", user.mail);
	    
	//Entrada
		var matricula = getValue("WKUser");
		var fields = new Array("colleagueName","login","mail");
		var constraints = new Array();
		var constraint = DatasetFactory.createConstraint("colleaguePK.colleagueId", matricula, matricula, ConstraintType.MUST);		
		var dataset;
		var username;
		var login;
	
	
	
	//Estrutura processamento
		constraints.push(constraint);
		dataset = DatasetFactory.getDataset("colleague", fields, constraints, null);
		username = dataset.getValue(0, "colleagueName");
		login = dataset.getValue(0,"login");
		mail = dataset.getValue(0,"mail");
	

	//Saida
		form.setValue("solicitante", username);
	//	form.setValue("matricula", matricula);
		form.setValue("login", login);
		form.setValue("mail", mail);
		form.setValue("Datadasolicitao", getCurrentDate());
		
		
		
/***************************************************************************************************************
* ****************  Tratando o e-mail e chamando o Dataset REST que traz os campos *****************************
****************************************************************************************************************/
		//Constraint do email
		var constraintemail = DatasetFactory.createConstraint("emailFluig", mail, mail, ConstraintType.MUST);	
		
		//Passando para Array
		var array1 = new Array(constraintemail);
		
		//Chamando o Dataset REST e enviando o e-mail (Array1)
		var emailretorno = DatasetFactory.getDataset("ds_teste_bancorbras_email", null, array1, null);
		
		log.info(">>> DisplayFields ConstraintEmail " +constraintemail + "array1 "+ array1);
		
		
		//form.setValue("CampoFormulario", emailretorno.getValue(0,"CampoREST"));
		
		form.setValue("NOME", emailretorno.getValue(0,"NOME"));
		log.info(">>> NOME " +emailretorno.getValue(0,"NOME"));

		form.setValue("DEPART", emailretorno.getValue(0,"DEPART"));
		log.info(">>> Departamento " +emailretorno.getValue(0,"DEPART"));
		
		form.setValue("DESDEPAR", emailretorno.getValue(0,"DESDEPAR"));
		log.info(">>> DESDEPAR " +emailretorno.getValue(0,"DESDEPAR"));

		form.setValue("empresa", emailretorno.getValue(0,"FILIAL"));
		log.info(">>> CDFILIAL " +emailretorno.getValue(0,"FILIAL"));

		form.setValue("MATRIC", emailretorno.getValue(0,"MATRIC"));
		log.info(">>> MATRIC " +emailretorno.getValue(0,"MATRIC"));

		form.setValue("TURTRAB", emailretorno.getValue(0,"TURTRAB"));
		log.info(">>> TURTRAB " +emailretorno.getValue(0,"TURTRAB"));

		form.setValue("EMPREST_ACT", emailretorno.getValue(0,"EMPREST_ACT"));
		log.info(">>> EMPREST_ACT " +emailretorno.getValue(0,"EMPREST_ACT"));

		form.setValue("FUNCAO", emailretorno.getValue(0,"FUNCAO"));
		log.info(">>> FUNCAO " +emailretorno.getValue(0,"FUNCAO"));

		form.setValue("GESTOR", emailretorno.getValue(0,"GESTOR"));
		log.info(">>> GESTOR " +emailretorno.getValue(0,"GESTOR"));

		form.setValue("EMAIL_GESTOR", emailretorno.getValue(0,"EMAIL_GESTOR"));
		log.info(">>> EMAIL_GESTOR " +emailretorno.getValue(0,"EMAIL_GESTOR"));

		form.setValue("UM_FER_STATUS", emailretorno.getValue(0,"UM_FER_STATUS"));
		log.info(">>> UM_FER_STATUS " +emailretorno.getValue(0,"UM_FER_STATUS"));

		form.setValue("UM_INI_DATABASE", emailretorno.getValue(0,"UM_INI_DATABASE"));
		log.info(">>> UM_INI_DATABASE " +emailretorno.getValue(0,"UM_INI_DATABASE"));

		form.setValue("UM_FIM_DATABASE", emailretorno.getValue(0,"UM_FIM_DATABASE"));
		log.info(">>> UM_FIM_DATABASE " +emailretorno.getValue(0,"UM_FIM_DATABASE"));

		form.setValue("UM_FER_A_VENCER", emailretorno.getValue(0,"UM_FER_A_VENCER"));
		log.info(">>> UM_FER_A_VENCER " +emailretorno.getValue(0,"UM_FER_A_VENCER"));

		form.setValue("UM_FER_DIREITO", emailretorno.getValue(0,"UM_FER_DIREITO"));
		log.info(">>> UM_FER_DIREITO " +emailretorno.getValue(0,"UM_FER_DIREITO"));

		form.setValue("UM_DT_INI_PROGUM", emailretorno.getValue(0,"UM_DT_INI_PROG1"));
		log.info(">>> UM_DT_INI_PROGUM " +emailretorno.getValue(0,"UM_DT_INI_PROG1"));

		form.setValue("UM_DIAS_PROGUM", emailretorno.getValue(0,"UM_DIAS_PROG1"));
		log.info(">>> UM_DIAS_PROGUM " +emailretorno.getValue(0,"UM_DIAS_PROG1"));

		form.setValue("UM_DIAS_AB_PROGUM", emailretorno.getValue(0,"UM_DIAS_AB_PROG1"));
		log.info(">>> UM_DIAS_AB_PROGUM " +emailretorno.getValue(0,"UM_DIAS_AB_PROG1"));

		form.setValue("UM_DT_INI_PROG_DOIS", emailretorno.getValue(0,"UM_DT_INI_PROG2"));
		log.info(">>> UM_DT_INI_PROG_DOIS " +emailretorno.getValue(0,"UM_DT_INI_PROG2"));

		form.setValue("UM_DIAS_PROG_DOIS", emailretorno.getValue(0,"UM_DIAS_PROG2"));
		log.info(">>> UM_DIAS_PROG_DOIS " +emailretorno.getValue(0,"UM_DIAS_PROG2"));

		form.setValue("UM_DIAS_AB_PROG_DOIS", emailretorno.getValue(0,"UM_DIAS_AB_PROG2"));
		log.info(">>> UM_DIAS_AB_PROG_DOIS " +emailretorno.getValue(0,"UM_DIAS_AB_PROG2"));

		form.setValue("UM_SALDO_DIAS_FER", emailretorno.getValue(0,"UM_SALDO_DIAS_FER"));
		log.info(">>> UM_SALDO_DIAS_FER " +emailretorno.getValue(0,"UM_SALDO_DIAS_FER"));

		form.setValue("DOIS_FER_STATUS", emailretorno.getValue(0,"DOIS_FER_STATUS"));
		log.info(">>> DOIS_FER_STATUS " +emailretorno.getValue(0,"DOIS_FER_STATUS"));

		form.setValue("DOIS_INI_DATABASE", emailretorno.getValue(0,"DOIS_INI_DATABASE"));
		log.info(">>> DOIS_INI_DATABASE " +emailretorno.getValue(0,"DOIS_INI_DATABASE"));

		form.setValue("DOIS_FIM_DATABASE", emailretorno.getValue(0,"DOIS_FIM_DATABASE"));
		log.info(">>> DOIS_FIM_DATABASE " +emailretorno.getValue(0,"DOIS_FIM_DATABASE"));

		form.setValue("DOIS_FER_A_VENCER", emailretorno.getValue(0,"DOIS_FER_A_VENCER"));
		log.info(">>> DOIS_FER_A_VENCER " +emailretorno.getValue(0,"DOIS_FER_A_VENCER"));

		form.setValue("DOIS_FER_DIREITO", emailretorno.getValue(0,"DOIS_FER_DIREITO"));
		log.info(">>> DOIS_FER_DIREITO " +emailretorno.getValue(0,"DOIS_FER_DIREITO"));

		form.setValue("DOIS_DT_INI_PROGUM", emailretorno.getValue(0,"DOIS_DT_INI_PROG1"));
		log.info(">>> DOIS_DT_INI_PROGUM " +emailretorno.getValue(0,"DOIS_DT_INI_PROG1"));

		form.setValue("DOIS_DIAS_PROGUM", emailretorno.getValue(0,"DOIS_DIAS_PROG1"));
		log.info(">>> DOIS_DIAS_PROGUM " +emailretorno.getValue(0,"DOIS_DIAS_PROG1"));
		
		form.setValue("DOIS_DIAS_AB_PROG1", emailretorno.getValue(0,"DOIS_DIAS_AB_PROG1"));
		log.info(">>> DOIS_DIAS_AB_PROG1 " +emailretorno.getValue(0,"DOIS_DIAS_AB_PROG1"));


		form.setValue("DOIS_DT_INI_PROG_DOIS", emailretorno.getValue(0,"DOIS_DT_INI_PROG2"));
		log.info(">>> DOIS_DT_INI_PROG_DOIS " +emailretorno.getValue(0,"DOIS_DT_INI_PROG2"));


		form.setValue("DOIS_DIAS_PROG_DOIS", emailretorno.getValue(0,"DOIS_DIAS_PROG2"));
		log.info(">>> DOIS_DIAS_PROG_DOIS " +emailretorno.getValue(0,"DOIS_DIAS_PROG2"));


		form.setValue("DOIS_DIAS_AB_PROG_DOIS", emailretorno.getValue(0,"DOIS_DIAS_AB_PROG2"));
		log.info(">>> DOIS_DIAS_AB_PROG_DOIS " +emailretorno.getValue(0,"DOIS_DIAS_AB_PROG2"));


		form.setValue("DOIS_SALDO_DIAS_FER", emailretorno.getValue(0,"DOIS_SALDO_DIAS_FER"));
		log.info(">>> DOIS_SALDO_DIAS_FER " +emailretorno.getValue(0,"DOIS_SALDO_DIAS_FER"));


		form.setValue("TRES_FER_STATUS", emailretorno.getValue(0,"TRES_FER_STATUS"));
		log.info(">>> TRES_FER_STATUS " +emailretorno.getValue(0,"TRES_FER_STATUS"));


		form.setValue("TRES_INI_DATABASE", emailretorno.getValue(0,"TRES_INI_DATABASE"));
		log.info(">>> TRES_INI_DATABASE " +emailretorno.getValue(0,"TRES_INI_DATABASE"));


		form.setValue("TRES_FIM_DATABASE", emailretorno.getValue(0,"TRES_FIM_DATABASE"));
		log.info(">>> TRES_INI_DATABASE " +emailretorno.getValue(0,"TRES_INI_DATABASE"));


		form.setValue("TRES_FER_A_VENCER", emailretorno.getValue(0,"TRES_FER_A_VENCER"));
		log.info(">>> TRES_INI_DATABASE " +emailretorno.getValue(0,"TRES_INI_DATABASE"));

		form.setValue("TRES_FER_DIREITO", emailretorno.getValue(0,"TRES_FER_DIREITO"));
		log.info(">>> TRES_FER_DIREITO " +emailretorno.getValue(0,"TRES_FER_DIREITO"));

		form.setValue("TRES_DT_INI_PROGUM", emailretorno.getValue(0,"TRES_DT_INI_PROG1"));
		log.info(">>> TRES_DT_INI_PROGUM " +emailretorno.getValue(0,"TRES_DT_INI_PROG1"));

		form.setValue("TRES_DIAS_PROGUM", emailretorno.getValue(0,"TRES_DIAS_PROG1"));
		log.info(">>> TRES_DIAS_PROGUM " +emailretorno.getValue(0,"TRES_DIAS_PROG1"));

		form.setValue("TRES_DIAS_AB_PROGUM", emailretorno.getValue(0,"TRES_DIAS_AB_PROG1"));
		log.info(">>> TRES_DIAS_AB_PROGUM " +emailretorno.getValue(0,"TRES_DIAS_AB_PROG1"));


		form.setValue("TRES_DT_INI_PROGDOIS", emailretorno.getValue(0,"TRES_DT_INI_PROG2"));
		log.info(">>> TRES_DT_INI_PROGDOIS " +emailretorno.getValue(0,"TRES_DT_INI_PROG2"));


		form.setValue("TRES_DIAS_PROGDOIS", emailretorno.getValue(0,"TRES_DIAS_PROG2"));
		log.info(">>> TRES_DIAS_PROGDOIS " +emailretorno.getValue(0,"TRES_DIAS_PROG2"));
		
		form.setValue("TRES_DIAS_AB_PROGDOIS", emailretorno.getValue(0,"TRES_DIAS_AB_PROG2"));
		log.info(">>> TRES_DIAS_AB_PROGDOIS " +emailretorno.getValue(0,"TRES_DIAS_AB_PROG2"));


		form.setValue("TRES_SALDO_DIAS_FER", emailretorno.getValue(0,"TRES_SALDO_DIAS_FER"));
		log.info(">>> TRES_SALDO_DIAS_FER " +emailretorno.getValue(0,"TRES_SALDO_DIAS_FER"));
	}
	
	form.setShowDisabledFields(true);
	form.setHidePrintLink(true);

		customHTML.append("<script> var CURRENT_STATE = "+getValue("WKNumState")+";</script>");
	
	
}

function getCurrentDate(){
	var df = new java.text.SimpleDateFormat("yyyy/MM/dd");
	var cal = java.util.Calendar.getInstance();
	return df.format(cal.getTime());
}
	
